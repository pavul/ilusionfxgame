package demolevel;


import com.bitless.graphicentity.Background;
import com.bitless.graphicentity.RectangleSprite;
import com.bitless.ntfc.LevelDrawable;
import com.bitless.level.BaseLevel;
import com.bitless.manager.GameManager;
import com.bitless.ntfc.Initiable;
import com.bitless.task.Task;
import com.bitless.util.ImageUtil;
import com.bitless.util.MathUtil;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;






    /**
     * in this level we play background and sound effect music
     */
  public  class DemoLevel10 extends BaseLevel
        implements LevelDrawable, Initiable
    {
      
     Background bg = new Background( ImageUtil.getImage( "bgimage.png" ));
      
     Task task;
     
     int x=0;
     
     int cc=0;
     
     int i = 0;
     
       
     List< RectangleSprite > rectList = new ArrayList<>();
       
     
        public DemoLevel10(int levelWidth, int levelHeight) 
        {
            super(levelWidth, levelHeight);
         
            initSound();
            
            GameManager.getPreRender().setFill( Color.BLUE );
            GameManager.getPreRender().fillRect(0,0,640,480);
            
            keyReleasedControl = e->
            {
                
                if(e.getCode() ==  KeyCode.A)
                    {
                        rectList.clear();
                    
                        for( i = 0; i < 10 ; i++ )
                        {
                        
                        System.err.println("Clear Rect");
                        rectList.add( 
                                new RectangleSprite(
                                        MathUtil.getRandomRange( 0, 600), 
                                        MathUtil.getRandomRange(0, 400), 10, 10) 
                                    );
                        }
                          
                    }
                
            };//keyreleased
            
            
            task = new Task();
            task.setCounter( 100 );
            
            
          
            
            
         }//
        

        @Override
        public void update( float l ) 
        {
            x++;
            cc++;
            System.err.println("--> "+cc);
            
            task.processTask(()->
            {
                x = 0;
                System.err.println("task executed in step "+cc);
                
                task.setCounter( 300 );
            });
            
            
        }//

        @Override
        public void preRender( GraphicsContext g ) 
        {
            drawBackground(g);
          
            drawForeground(g);
            
            drawHUD(g);
   
//            g.save();
           
//            g.restore();
            
            
            rectList.forEach( r -> 
            {
            g.clearRect( r.getX(), r.getY(), r.getW(), r.getH() );
            });
            
        }//

    @Override
    public void drawBackground(GraphicsContext g) 
    {
        
        //g.clearRect(0,0, 10*100,levelHeight);
        
//        Effect flash = new GaussianBlur( 20 );
//        gm.getG().setGlobalBlendMode(BlendMode.MULTIPLY);
        
//        g.save();
        //bg.draw( g );
       
//        g.setFill(Color.BLACK);
//        g.fillRect( 0, 0,  levelWidth, levelHeight);
//        g.setEffect( new Blend( BlendMode.OVERLAY ) );
//        g.save();
//        g.setFill(Color.WHITE);
//        g.fillOval(100, 100, 100, 100);
//        g.setEffect( new Blend( BlendMode.SCREEN ) );
        
//        g.restore();
//        g.save();
//        
//        g.restore();
        
//        bg.draw( g );
//        
//        g.save();
//        
//        g.setFill(Color.BLACK);
//        g.fillRect( 0, 0,  levelWidth, levelHeight);
//        g.clearRect(20, 20, 200,200);
//        
//        g.restore();
    }

    @Override
    public void drawForeground(GraphicsContext g) 
    {
            
    }

    @Override
    public void drawHUD(GraphicsContext g) 
    {
       
        g.setFill(Color.RED);

        g.fillRect(x, 100, 50, 50 );
    }


    @Override
    public void initData() 
    {
    }

    @Override
    public void initSound() 
    {
        
        audioManager.loadSfx( "audio1", "buttonerror.wav" );
        
    }//

    @Override
    public void initImages() {
    }

    @Override
    public void initSprite() {
    }

    @Override
    public void initBackground() {
    }

    @Override
    public void initForeground() {
    }

    @Override
    public void initHUD() {
    }
        
     @Override
    public void postRender(GraphicsContext g) 
    {
    }
    }//
    
    
    
    