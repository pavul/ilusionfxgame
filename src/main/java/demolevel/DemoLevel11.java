package demolevel;

import com.bitless.input.GamepadInputButton;
import com.bitless.input.GamePad;
import com.bitless.ntfc.LevelDrawable;
import com.bitless.level.BaseLevel;
import com.bitless.net.Server;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

    /**
     * in this level we play with UDP server connections, 
     * the class to use this is com.bitless.net.Server
     */
  public  class DemoLevel11 extends BaseLevel
                            implements LevelDrawable
    {
      
        Server s;
        
        /**
         * instance to get gamepad input
         */
        GamePad gamePad;
        
        public DemoLevel11(int levelWidth, int levelHeight) 
        {
            super(levelWidth, levelHeight);
            
           
            try 
            {
                System.err.println("starting server");
                s = new Server( 24586 );
                
                //accept incoming conenctions
                s.connect();
         
            } 
            catch (IOException ex) 
            {
                Logger.getLogger(DemoLevel.class.getName()).log(Level.SEVERE, "::: error con algo", ex);
            }
         
         
            gamePad = new GamePad();

         
         }//
        

        @Override
        public void update( float l ) 
        {
            
            
            gamePad.processGamepadInput(e ->
            {
                
                
                
                if( e.getName().equals( GamepadInputButton.GEN_BTN_0 ) )
                {
                    
                    if( e.getValue() ==  GamepadInputButton.DIG_PRESSED_VALUE )
                    {
                        System.err.println("Button 0 Pressed");
                    }
                    else
                    {
                        System.err.println("Button 0 Released");
                    }
                    
                    
                    System.err.println("button 0 event: "+e.getValue() );    
                }
                
                if( e.getName().equals(GamepadInputButton.Z_AXIS ) )
                    {
                        System.err.println("Z AXIS "+e.getValue());
                    }
                
                
               if( gamePad.isDPadUpPressed(e) )
                {
                 System.err.println( "DPAD UP" );
                }
               else
                {
                 System.err.println( "DPAD UP RELEASED" );
                }
               
               if( gamePad.isDPadDownPressed(e) )
               {
                System.err.println("DPAD DOWN ");
               }
     
            });
                
            
        }//

        @Override
        public void preRender( GraphicsContext g ) 
        {
            drawBackground(g);
          
            drawForeground(g);
            
            drawHUD(g);
   
        }//

    @Override
    public void drawBackground(GraphicsContext g) 
    {
        
        //g.clearRect(0,0, 10*100,levelHeight);
       
        g.setFill(Color.BLACK);
        g.fillRect( 0, 0,  levelWidth, levelHeight);
        
    }

    @Override
    public void drawForeground(GraphicsContext g) 
    {
            
        g.setFill( Color.WHITE );
        g.fillText( "Server UDP network Example" , 20, 40 );
        
        g.fillText( "clients connected "+s.getClientList().size(), 20, 70);
        
    }

    @Override
    public void drawHUD(GraphicsContext g) 
    {
    }

     @Override
    public void postRender(GraphicsContext g) 
    {
    }
    
  }//class
 