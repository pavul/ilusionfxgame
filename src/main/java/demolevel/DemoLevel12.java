package demolevel;

import com.bitless.ntfc.LevelDrawable;
import com.bitless.level.BaseLevel;
import com.bitless.net.Client;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

    /**
     * in this level we play with UDP server connections, 
     * the class to use this is com.bitless.net.Server
     */
  public  class DemoLevel12 extends BaseLevel
        implements LevelDrawable
    {
      
       
        Client client;
            
        public DemoLevel12(int levelWidth, int levelHeight) 
        {
            super(levelWidth, levelHeight);
            
            
            try 
            {
                client = new Client( "127.0.0.1" , 24586 );
                
                System.err.println("waiting to connect to the server");
                client.connect();
                
            } 
            catch ( SocketException ex ) 
            {
                Logger.getLogger(DemoLevel.class.getName()).log(Level.SEVERE, "::: error con algo", ex);
            }
         
        
         }//
        

        @Override
        public void update( float l ) 
        {
            
        }//

        @Override
        public void preRender( GraphicsContext g ) 
        {
            drawBackground(g);
          
            drawForeground(g);
            
            drawHUD(g);
   
            
        }//

    @Override
    public void drawBackground(GraphicsContext g) 
    {
        
        //g.clearRect(0,0, 10*100,levelHeight);
       
        g.setFill(Color.BLACK);
        g.fillRect( 0, 0,  levelWidth, levelHeight);
        
    }

    @Override
    public void drawForeground(GraphicsContext g) 
    {
            
        g.setFill( Color.WHITE );
        g.fillText( "Client UDP network Example" , 20, 40 );
        g.fillText( "Client ID: " + client.getId() , 20, 70 );
        
        
    }

    @Override
    public void drawHUD(GraphicsContext g) 
    {
    }


     @Override
    public void postRender(GraphicsContext g) 
    {
    }
    
  }//class
 