package demolevel;

import com.bitless.input.GamePad;
import com.bitless.input.GamepadInputButton;
import com.bitless.level.BaseLevel;
import com.bitless.manager.GameManager;
import com.bitless.util.GameUtil;
import game.arkanoid.Arkanoid;
import game.asteroids.Asteroids;
import game.mazegame.MazeGame;
import game.tictactoe.Tictactoe;
import game.pong.Pong;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;

/**
 * in this level we create sprites and check collision between them, also text
 * is display using default font, it also set implementation for key listeners
 * so the sprite can be controlable thru keyboad
 * 
 * IT also have gamepad implementation, this should work with a game pad
 * connected
 */
public class LevelMenu extends BaseLevel 
{

    /**
     * to select a demolevel to play
     */
    int demoLevelSelector = 3;

    /**
     * to select a game to play
     */
    int gameSelector = 3;

    /**
     * if true pointer will be on DEMOLEVEL if false pointer will be on
     * GAMELEVEL
     */
    boolean modeSelector = true;

    final int LAST_DEMOLEVEL = 17;
    final int LAST_GAMELEVEL = 7;

    
    GamePad gamepad;
    
    public LevelMenu( int levelWidth, int levelHeight )
    {
        super(levelWidth, levelHeight);
        keyReleasedControl
                = e -> {

                    if (e.getCode().equals(KeyCode.W)) {
                        if (modeSelector) {
                            demoLevelSelector--;
                            if (demoLevelSelector <= 3) {
                                demoLevelSelector = 3;
                            }
                        } else {
                            gameSelector--;
                            if (gameSelector <= 3) {
                                gameSelector = 3;
                                
                            }
                        }

                    }//

                    if (e.getCode().equals(KeyCode.S)) {

                        if ( modeSelector ) 
                        {
                            demoLevelSelector++;
                            if (demoLevelSelector >= LAST_DEMOLEVEL) {
                                demoLevelSelector = LAST_DEMOLEVEL;
                            }
                        } 
                        else 
                        {
                            gameSelector++;
                            if (gameSelector >= LAST_GAMELEVEL) {
                                gameSelector = LAST_GAMELEVEL;
                            }
                        }

                    }

                    //enter released
                    if (e.getCode().equals(KeyCode.ENTER)) 
                    {
                        if (modeSelector) {
                            gotoDemoLevel(demoLevelSelector);
                        } else {
                            gotoGameLevel(gameSelector);
                        }

                    }

                    if (e.getCode().equals(KeyCode.A)) {
                        modeSelector = true;
                    }

                    if (e.getCode().equals(KeyCode.D)) {
                        modeSelector = false;
                    }

                };

        gamepad = new GamePad();
        
    }//

    @Override
    public void update(float l) 
    {

        gamepad.processGamepadInput(e->
        {
        
            if( gamepad.isDPadDownPressed(e) )
            {
                        if ( modeSelector ) 
                        {
                            demoLevelSelector++;
                            if (demoLevelSelector >= LAST_DEMOLEVEL) {
                                demoLevelSelector = LAST_DEMOLEVEL;
                            }
                        } 
                        else 
                        {
                            gameSelector++;
                            if (gameSelector >= LAST_GAMELEVEL) {
                                gameSelector = LAST_GAMELEVEL;
                            }
                        }
            }//
            
             if( gamepad.isDPadUpPressed(e) )
            {
                 if (modeSelector) {
                    demoLevelSelector--;
                    if (demoLevelSelector <= 3) {
                        demoLevelSelector = 3;
                    }
                } else {
                    gameSelector--;
                    if (gameSelector <= 3) {
                        gameSelector = 3;

                    }
                }
            }//
        
             //push X button on PS4, A button on XBOX one 
             if( gamepad.getDigitalValue( e, GamepadInputButton.GEN_BTN_1, 0 ) )
                {
                    if (modeSelector) 
                    {
                        gotoDemoLevel(demoLevelSelector);
                    } 
                    else 
                    {
                        gotoGameLevel(gameSelector);
                    }
                }
             
             
        });
        
        
    }

    @Override
    public void preRender(GraphicsContext g) 
    {
        g.setFill(Color.BLACK);
        g.fillRect(0, 0, levelWidth, levelHeight);

        //TUTORIALS
        g.setFill(Color.YELLOW);
        g.fillText("TUTORIALS", 20, 20);

        g.setFill(Color.WHITE);
        g.fillText("Demo Level", 20, 60);
        g.fillText("Demo Level2", 20, 80);
        g.fillText("Demo Level3", 20, 100);
        g.fillText("Demo Level4", 20, 120);
        g.fillText("Demo Level5", 20, 140);
        g.fillText("Demo Level6", 20, 160);
        g.fillText("Demo Level7", 20, 180);
        g.fillText("Demo Level8", 20, 200);
        g.fillText("Demo Level9", 20, 220);
        g.fillText("Demo Level0", 20, 240);
        g.fillText("Demo Level11 (Server)", 20, 260);
        g.fillText("Demo Level12 (Client)", 20, 280);
        g.fillText("Demo Level13 jInput", 20, 300);
        g.fillText("Demo Level14 (GameData)", 20, 320);
        g.fillText("exit", 20, 340);

        //GAMES
        g.setFill(Color.YELLOW);
        g.fillText("GAMES", 320, 20);
        g.setFill(Color.WHITE);
        g.fillText("Asteroids", 320, 60);
        g.fillText("Arkanoid",  320, 80);
        g.fillText("Maze Game", 320, 100);
        g.fillText("Ping Pong", 320, 120);
        g.fillText("TicTacToe", 320, 140);
        
        
        if(modeSelector)
        {
        g.fillOval(10, (20 * demoLevelSelector) - 6, 6, 6);
        
        }
        else
        {
        g.fillOval(310, (20 * gameSelector) - 6, 6, 6);
        }

    }//

    private void gotoDemoLevel(int nmbr) {

        switch (nmbr) {
            case 3:
                gm.loadLevel(new DemoLevel(640, 480));
                break;
            case 4:
                gm.loadLevel(new DemoLevel2(256, 224));
                break;
            case 5:
                gm.loadLevel(new DemoLevel3(640, 480));
                break;
            case 6:
                gm.loadLevel(new DemoLevel4(640, 480));
                break;
            case 7:
                gm.loadLevel(new DemoLevel5(640, 480));
                break;
            case 8:
                gm.loadLevel(new DemoLevel6(640, 480));
                break;
            case 9:
                gm.loadLevel(new DemoLevel7(640, 480));
                break;
                case 10:
                  gm.loadLevel(new DemoLevel8(640, 480, 320, 240));
                    GameManager.getPreRender().scale(2, 2);
                break;
                case 11:
                  gm.loadLevel(new DemoLevel9( 640, 480 ) );
                break;
            case 12:
                gm.loadLevel(new DemoLevel10( 640, 480 ) );
                break;
            case 13:
                gm.loadLevel(new DemoLevel11( 640, 480 ) );
                break;
            case 14:
                gm.loadLevel(new DemoLevel12( 640, 480 ) );
                break;
            case 15:
                gm.loadLevel(new DemoLevel13( 640, 480 ) );
                break;
            case 16:
                gm.loadLevel(new DemoLevel14( 640, 480 ) );
                break;
            case 17:
                GameUtil.closeGame();
                break;

        }//suich

    }

    private void gotoGameLevel( int game ) 
    {

        switch ( game ) 
        {
            case 3:
                gm.loadLevel(new Asteroids(640, 480));
                break;
            case 4:
                gm.loadLevel(new Arkanoid(640, 480));
                break;
            case 5:
                gm.loadLevel(new MazeGame(640, 480));
                break;
            case 6:
                gm.loadLevel(new Pong(640, 480));
            break;
            case 7:
                gm.loadLevel(new Tictactoe(640, 480));
                break;
        }//

    }//

     @Override
    public void postRender(GraphicsContext g) 
    {
    }
    
}//