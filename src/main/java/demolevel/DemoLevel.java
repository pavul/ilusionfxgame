package demolevel;

import com.bitless.graphicentity.BaseSprite;
import com.bitless.graphicentity.Sprite;
import com.bitless.level.BaseLevel;
import com.bitless.ntfc.LevelDrawable;
import com.bitless.util.CollisionUtil;
import com.bitless.util.GameUtil;
import com.bitless.util.ImageUtil;
import com.bitless.util.SpritetUtil;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;

    /**
     * in this level we create sprites and 
     * check collision between them, also text 
     * is display using default font,
     * it also set implementation for key listeners
     * so the sprite can be controlable thru keyboad
     * 
     * THINGS TO UNDERSTAND IN THIS LEVEL:
     * sprite creation
     * sprite movement
     * sprite rotation
     * sprite movement towards other sprite
     * sprite movement in certain direction (angle)
     * use of Util class
     * use of SpriteUtil class
     * use of CollisionManager to make collisions
     */
  public  class DemoLevel extends BaseLevel
                          implements LevelDrawable
    {
        
        Image[] frames =  ImageUtil.getImageFrames( 3, 32,32,"square.png" );
        
        Sprite player; //= new Sprite( frames[ 0 ] );
        Sprite enemy; // = new Sprite( frames[ 0 ]  );
        Sprite bullet; //= new Sprite( frames[ 0 ]  );
        Sprite arrow;
        
        
        double xTrans = 0;
        
        boolean moveRight;
        boolean moveLeft;
        boolean moveUp;
        boolean moveDown;
        
        int spd = 2;
        
        boolean moveFloor = false;
        
        float volume = 1;
        boolean muted = false;
        
        
        
        
        
        
        
        public DemoLevel(int levelWidth, int levelHeight) 
        {
            super(levelWidth, levelHeight);
      
         //player creation
         player = new Sprite( frames[ 0 ] );
         player.setPosition( 20, 20 );
         //player gonna move to 45 degress direction (clockwise)
         //but that angle wont be set to player angle variable
//         SpritetUtil.moveToAngle( player, 90, spd, false );
         
         
         //enemy sprite is created to have collisions with player, 
//         this will be pushed by the player when colliding
         enemy = new Sprite( frames[ 1 ]  );
         enemy.setPosition( 100, 100 );
         enemy.setLabel( "enemy" );
         
         //bullet is an object that will be launched towars player
         //from center of the level, once reach outside visible are
         //will return to middle center of the level
         bullet = new Sprite( ImageUtil.getImage( "bullet.png" )  );
         bullet.setPosition( 100, 300 );
         bullet.setLabel( "enemy" );
         
         
         //this object will rotate towards player
         arrow = new Sprite( ImageUtil.getImage( "arrow.png" )  );
         arrow.setPosition( this.levelWidth/2, this.levelHeight/2);
         arrow.setLabel("enemy");
         
         
//         input controls
         keyPressedControl =
                e->{
                    
                        System.err.println("key pressed at: "+e.getCode());

                        if( e.getCode().equals(  KeyCode.A ))
                        {
                            moveLeft = true;
                        }

                        if( e.getCode().equals(  KeyCode.D ))
                        {
                            moveRight = true;
                        }
                        
                        if( e.getCode().equals(  KeyCode.W ))
                        {
                            moveUp = true;
                        }

                        if( e.getCode().equals(  KeyCode.S ))
                        {
                            moveDown = true;
                        }
                    
                   };
         
         
         keyReleasedControl = 
                 e->{  
                 
                  if( e.getCode().equals(  KeyCode.A ))
                    {
                        moveLeft = false;
                    }
                 
                    if( e.getCode().equals(  KeyCode.D ))
                    {
                        moveRight = false;
                    }   
                    
                     
                        if( e.getCode().equals(  KeyCode.W ))
                        {
                            moveUp = false;
                        }

                        if( e.getCode().equals(  KeyCode.S ))
                        {
                            moveDown = false;
                        }
                        
                        if( e.getCode().equals(  KeyCode.SPACE ))
                        {
                            bullet.setPosition( this.levelWidth/2, this.levelHeight/2 );
                            SpritetUtil.moveTo(bullet, 
                                    player.getX() + player.getW()/2,
                                    player.getY() + player.getH()/2, 3);
                        }
                        
                       
                        
                        if( e.getCode().equals(  KeyCode.N ))
                        {
                           BaseSprite sss = SpritetUtil.spriteNearest(player, spriteList,
                                    0, 0, levelWidth, levelHeight );
                            
                            System.err.println("NEAREST = "+sss.getX() );
                            
                        }
                        
                        
                 };
    
        
         
        }//

        @Override
        public void update( float l ) 
        {
               
            //movement of player
            if(moveRight)  player.moveX( spd );
            if(moveLeft)  player.moveX( -spd );
            if(moveUp)  player.moveY( -spd );
            if(moveDown)  player.moveY( spd );

//            System.err.println("==> "+ colMan.rectangleColision( player, enemy)  );

         
            GameUtil.screenShake( 0 );



//METHOD2
            if( CollisionUtil.rectangleColision(player, enemy) )
            {
                System.err.println("COL TRUE;");
            }
         

            System.err.println("Collision with Player on "+  
                    CollisionUtil.rectangleColision( player, enemy, true ) );
            

            //update bullet movement
         bullet.moveXSpd();
         bullet.moveYSpd();
            
         //update player movement
         player.moveXSpd();
         player.moveYSpd();
         
//         arrow.setRotation( arrow.getRotation() + 100 );
         
//test if bullet is inside visible level area, 
//if not then will be set in middle of level again
         if( !CollisionUtil.isInside(
                 bullet.getX(), bullet.getY(), bullet.getW(), bullet.getH(),
                 0, 0, this.levelWidth, this.levelHeight)  )
         {
         bullet.setSpdX( 0 );
         bullet.setSpdY( 0 );
         bullet.setPosition(this.levelWidth/2, this.levelHeight/2);
         }
         

         //this will set the angle of the arrow pointing to player direction
        arrow.setAngle(SpritetUtil.getAngle(arrow, 
                player.getX() + player.getW()/2,
                player.getY() + player.getH()/2
        ) );

        }//update

        @Override
        public void preRender( GraphicsContext g ) 
        {
            
            drawBackground(g);
          
            drawForeground(g);
            
            drawHUD(g);
   
        }//

    @Override
    public void postRender(GraphicsContext g) 
    {
    }
    
        
    @Override
    public void drawBackground(GraphicsContext g) 
    {
        
        g.clearRect(0,0, levelWidth * gm.getxScale() ,levelHeight * gm.getyScale() );
            
            g.setFill( Color.BLUE );
            for( int i =0 ; i < 10 ; i++)
            {
            g.fillText(""+(i*100), ( i * 100 ), 20);
            }
        
    }

    @Override
    public void drawForeground(GraphicsContext g) 
    {
            player.draw(g);
            enemy.draw(g);
            bullet.draw(g);  
            
            
            //draws rotating arrow
            arrow.draw(g);
            
    }

    @Override
    public void drawHUD(GraphicsContext g) 
    {
        g.setFill(Color.RED);
        g.fillText("TEST TEXT",20,20);    
        g.setFill(Color.BURLYWOOD);
        g.fillText( "this font comes from Library" , 20,60);
    }

  
        
    }//