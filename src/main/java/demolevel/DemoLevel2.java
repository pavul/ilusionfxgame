package demolevel;

import com.bitless.ntfc.LevelDrawable;
import com.bitless.collision.Collider;
import com.bitless.level.BaseLevel;
import com.bitless.graphicentity.Sprite;
import com.bitless.graphicentity.Tile;
import com.bitless.ntfc.Initiable;
import com.bitless.util.ImageUtil;
import com.bitless.util.TileUtil;
import java.util.List;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;

    /**
     * @TODO not possible to fix this level yet, we have to implement
     * a good tile Util class for this
     * in this level we create sprites and 
     * check collision between them, also text 
     * is display using default font,
     * it also set implementation for key listeners
     * so the sprite can be controlable thru keyboad
     */
  public  class DemoLevel2 extends BaseLevel
        implements LevelDrawable, Initiable
    {
        
        //containing tiles maps
//        TileDemoLevel2Maper tileDemoLevel2;
        
        List<Tile> tileList;
        
        List<Collider> colliderList;
        
        //util class to parse tiles
        TileUtil tileUtil;
         Image[] bgTiles = null;
        Sprite player; //= new Sprite( frames[ 0 ] );
        Sprite floor; //= new Sprite( frames[ 0 ]  );
        
        double xTrans = 0;
        
        
        boolean moveRight;
        boolean moveLeft;
        boolean moveUp;
        boolean moveDown;
        
        int spd = 2;
        
        float rot = 2;
        float al =1;
        
        int xtext = 0;
        
        public DemoLevel2(int levelWidth, int levelHeight) 
        {
            super(levelWidth, levelHeight);
            
         
         keyPressedControl =
                e->{
                    
//                        System.err.println("key pressed at: "+e.getCode());

                        if( e.getCode().equals(  KeyCode.A ))
                        {
                            moveLeft = true;
                        }

                        if( e.getCode().equals(  KeyCode.D ))
                        {
                            moveRight = true;
                        }
                        
                        if( e.getCode().equals(  KeyCode.W ))
                        {
                            moveUp = true;
                        }

                        if( e.getCode().equals(  KeyCode.S ))
                        {
                            moveDown = true;
                        }
                    
                   };
         
         
         keyReleasedControl = 
                 e->{  
                 
                  if( e.getCode().equals(  KeyCode.A ))
                    {
                        moveLeft = false;
                    }
                 
                    if( e.getCode().equals(  KeyCode.D ))
                    {
                        moveRight = false;
                    }   
                    
                     
                        if( e.getCode().equals(  KeyCode.W ))
                        {
                            moveUp = false;
                        }

                        if( e.getCode().equals(  KeyCode.S ))
                        {
                            moveDown = false;
                        }
                 
                 };
         
       //util class to parse/draw tiles
       tileUtil = new TileUtil();
       
               initBackground();

        }//

        @Override
        public void update( float l ) 
        {
               
            
//            if(moveRight)  player.moveX( spd );
//            if(moveLeft)  player.moveX( -spd );
//            if(moveUp)  player.moveY( -spd );
//            if(moveDown)  player.moveY( spd );
//
//            
//            //tile collision
//            //colMan.tileCollision( player, colliderList );
//           
//           
//           for( int i = 0; i < colliderList.size()  ;i++)
//           {
//               Collider cc = (Collider)colliderList.get( i );
//               
//               colMan. tileCollision2( 
//                       player, 
//                       cc.getCx(),
//                       cc.getCy(),
//                       cc.getCw(),
//                       cc.getCh()
//               );
//               
//           }//
//            
//           
//           player.setRotation( rot );
//           
//           player.setAlpha(al);
//           rot+=90;
//           al-=0.001;
//            player.setAlpha(0.5f);
            
        }

        @Override
        public void preRender( GraphicsContext g ) 
        {
        //            switch(gameState)
        //            {
        //                case PLAYING:
        //                    
                    drawBackground(g);
                    drawForeground(g);
                    drawHUD(g);
        //                break;
        //            }//suich   
        }//

    @Override
    public void drawBackground(GraphicsContext g) 
    {
        
        g.clearRect(0,0, levelWidth , levelHeight );
        
    //        g.setFill( Color.WHITE );
    //        g.fillRect(0,0, levelWidth, levelHeight);
        
        tileUtil.drawTiles( g, bgTiles, tileList  );
 
        
    }

    @Override
    public void drawForeground(GraphicsContext g) 
    {
//            player.draw(g);
////            enemy.draw(g);
////            floor.draw(g);  
//            
//              g.setFill(Color.PERU);
//            g.fillRect(
//                    player.getX()+player.getCollider().getCx()  ,
//                    player.getY()+player.getCollider().getCy()  ,
//                    player.getCollider().getCw()  ,
//                    player.getCollider().getCh() 
//                    );
////            
//            
//            enemy.draw( g );
            
//            g.setFill(Color.PINK);
//            g.fillRect(
//                    enemy.getX()+enemy.getCollider().getCx()  ,
//                    enemy.getY()+enemy.getCollider().getCy()  ,
//                    enemy.getCollider().getCw()  ,
//                    enemy.getCollider().getCh() 
//                    );
//            
            
    }//

    @Override
    public void drawHUD(GraphicsContext g) 
    {
        xtext+=1;
        g.setFill(Color.RED);
        g.fillText("NEW TEXT",xtext,200);
    }

    @Override
    public void initData() 
    {
    }

    @Override
    public void initSound() {
     }

    @Override
    public void initSprite() {
     }

    @Override
    public void initBackground() 
    {
               bgTiles =  ImageUtil.getImageFrames( 6, 32,32,"bgtiles.png" );
                
                int []tileMap =
                {
                    3,3,3,3,3,3,3,3,
                    2,2,2,2,2,2,2,2,
                    2,2,2,2,2,2,2,2,
                    2,2,2,2,2,2,2,2,
                    2,2,2,2,2,2,2,2,
                    1,1,1,1,1,1,1,1,
                    0,0,0,0,0,0,0,0
                }; 
                
                tileList = tileUtil.parseTiles(tileMap, 32,32, 7, 8 );
               
     }//

    @Override
    public void initForeground() {
     }

    @Override
    public void initHUD() {
     }

    @Override
    public void initImages() {
     }

 @Override
    public void postRender(GraphicsContext g) 
    {
    }
        
        
    }//
    
    
    
    