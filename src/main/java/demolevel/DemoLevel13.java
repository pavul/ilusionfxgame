package demolevel;

import com.bitless.graphicentity.Background;
import com.bitless.graphicentity.Sprite;
import com.bitless.input.GamepadInputButton;
import com.bitless.input.GamePad;
import com.bitless.ntfc.LevelDrawable;
import com.bitless.level.BaseLevel;
import com.bitless.util.ImageUtil;
import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javax.rmi.CORBA.Util;

    /**
     * DEMO for gamepad
     */
  public  class DemoLevel13 extends BaseLevel
        implements LevelDrawable
    {
      
      
      Background bg[];//white squares to show diferent axis working
      Sprite button[];//red buttons that turn red when some controller button is pressed
      
      
      Point2D xAxisCursor;
      Point2D yAxisCursor;
      Point2D zAxisCursor;
      
      Point2D xRotationCursor;
      Point2D yRotationCursor;
      Point2D zRotationCursor;
      
      
      float xMovCursor;
      float yMovCursor;
      float zMovCursor;
      
      float xRotMovCursor;
      float yRotMovCursor;
      float zRotMovCursor;
      
      String dpadPressed ="";
      
      private int step = 0;
      
        /**
         * instance to get gamepad input
         */
        GamePad gamePad;
        
        public DemoLevel13(int levelWidth, int levelHeight) 
        {
            super(levelWidth, levelHeight);

            Image whiteSquare = ImageUtil.getImage( "whitesquare.png" );
            
            Image []redButton = ImageUtil.getImageFrames( 2, 32,32, "redbutton.png" );
            
            bg = new Background[ 6 ];
            
            bg[ 0 ] = new Background( whiteSquare );
            bg[ 0 ].setPosition( 50, 40 );
            
            bg[ 1 ] = new Background( whiteSquare );
            bg[ 1 ].setPosition( 120, 40 );
            
            bg[ 2 ] = new Background( whiteSquare );
            bg[ 2 ].setPosition( 50, 110 );
            
            bg[ 3 ] = new Background( whiteSquare );
            bg[ 3 ].setPosition( 120, 110 );
            
            bg[ 4 ] = new Background( whiteSquare );
            bg[ 4 ].setPosition( 50, 180 );
            
            bg[ 5 ] = new Background( whiteSquare );
            bg[ 5 ].setPosition( 120, 180 );
            
             //buttons to activate when pressed
            button = new Sprite[ 15 ];
            for( int i =0 ; i < button.length ; i++)
            {
            button[ i ] = new Sprite( redButton );
            }
            button[ 0 ].setPosition( 250 , 60 );
            button[ 1 ].setPosition( 290 , 60 );
            button[ 2 ].setPosition( 330 , 60 );
            button[ 3 ].setPosition( 370 , 60 );
            button[ 4 ].setPosition( 410 , 60 );
            button[ 5 ].setPosition( 250, 100 );
            button[ 6 ].setPosition( 290, 100 );
            button[ 7 ].setPosition( 330, 100 );
            button[ 8 ].setPosition( 370, 100 );
            button[ 9 ].setPosition( 410, 100 );
            button[ 10 ].setPosition( 250,140 );
            button[ 11 ].setPosition( 290,140 );
            button[ 12 ].setPosition( 330,140 );
            button[ 13 ].setPosition( 370,140 );
            button[ 14 ].setPosition( 410,140 );



            
        xAxisCursor= 
                   new Point2D( bg[ 0 ].getX()+32, bg[ 0 ].getY() + 32 );
       
        yAxisCursor= 
                   new Point2D( bg[ 2 ].getX()+ 32,  bg[ 2 ].getY() + 32);
        
        zAxisCursor= 
                       new Point2D( bg[ 4 ].getX()+ 32,  bg[ 4 ].getY() + 32 );
       
       xRotationCursor= 
                   new Point2D( bg[ 1 ].getX()+ 32, bg[ 1 ].getY() + 32 );
       
       yRotationCursor= 
                       new Point2D( bg[ 3 ].getX()+ 32, bg[ 3 ].getY() + 32 );
       
       zRotationCursor= 
                       new Point2D( bg[ 5 ].getX()+ 32, bg[ 5 ].getY() + 32 );
      
            
            //instance to manage controller input            
            gamePad = new GamePad();
            
//            gamePad.setxAxisThreshold( 0.05f);
//            gamePad.setyAxisThreshold( 0.05f );
//            gamePad.setzAxisThreshold( 0.05f );

         }//
        

        @Override
        public void update( float l ) 
        {
            
            
            gamePad.processGamepadInput(e ->
            {
                
                for( int i=0; i < 15; i++ )
                {

                    if( e.getName().contains( String.valueOf( i ) ) )
                    {
                    button[ i ].setFrameIndex( 1 );
                     System.err.println("Event: "+e.getName()+" : "+e.getValue() );
                    }
                    else
                    {
                    button[ i ].setFrameIndex( 0 );
                    }

                }
                
                if( e.getName().equals( GamepadInputButton.X_AXIS) )
                {
                if( e.getValue() != GamepadInputButton.EMPTY_VALUE)
                    {
                       System.err.println(step+": XAXIS: "+gamePad.getXAxis(e));
//                           System.err.println("YAXIS: "+gamePad.getYAxis(e));
                        xMovCursor+= e.getValue() *10;
                    }
                }
                
                else
                if( e.getName().equals( GamepadInputButton.Y_AXIS) )
                {
                if( e.getValue() != GamepadInputButton.EMPTY_VALUE)
                    {
                       System.err.println("YAXIS: "+gamePad.getXAxis(e));
//                           System.err.println("YAXIS: "+gamePad.getYAxis(e));
                        yMovCursor+= e.getValue() *10;
                    }
                }
               
               
                else
                 if( e.getName().equals( GamepadInputButton.Z_AXIS) )
                {
                    if( e.getValue() != GamepadInputButton.EMPTY_VALUE)
                    {
                       System.err.println("ZAXIS: "+gamePad.getZAxis(e));
                       zMovCursor+= e.getValue() *10;
                    }
                }
                 
                 else
                 if( e.getName().equals( GamepadInputButton.X_ROTATION) )
                {
                    if( e.getValue() != GamepadInputButton.EMPTY_VALUE)
                    {
                       System.err.println("X ROT: "+gamePad.getZAxis(e));
                       xRotMovCursor+= e.getValue()*10;
                    }
                }
                
                else
                 if( e.getName().equals( GamepadInputButton.Y_ROTATION) )
                {
                    if( e.getValue() != GamepadInputButton.EMPTY_VALUE)
                    {
                       System.err.println("Y ROT: "+gamePad.getZAxis(e));
                       yRotMovCursor+= e.getValue()*10;
                    }
                }
                 
                else
                 if( e.getName().equals( GamepadInputButton.Z_ROTATION) )
                {
                    if( e.getValue() != GamepadInputButton.EMPTY_VALUE)
                    {
                       System.err.println("Z ROT: "+gamePad.getZAxis(e));
                       zRotMovCursor = e.getValue()*10;
                    }
                }
                
   
                 
                 if( e.getName().equals( GamepadInputButton.DPAD ) )
                    {
                        System.err.println("EVENT: "+e.getValue() );
                    
                        if( e.getValue() == GamepadInputButton.DPAD_UP_BTN )
                        {dpadPressed ="DPAD UP";}
                        else if(e.getValue() == GamepadInputButton.DPAD_RIGHT_BTN)
                        {dpadPressed ="DPAD RIGTH";}
                        else if(e.getValue() == GamepadInputButton.DPAD_DOWN_BTN)
                        {dpadPressed ="DPAD DOWN";}
                        else if(e.getValue() == GamepadInputButton.DPAD_LEFT_BTN)
                        {dpadPressed ="DPAD LEFT";}
                        else if(e.getValue() == GamepadInputButton.DPAD_UPLEFT_BTN)
                        {dpadPressed ="DPAD UP-LEFT";}
                        else if(e.getValue() == GamepadInputButton.DPAD_UPRIGHT_BTN)
                        {dpadPressed ="DPAD UP-RIGTH";}
                        else if(e.getValue() == GamepadInputButton.DPAD_DOWNLEFT_BTN)
                        {dpadPressed ="DPAD DOWN-LEFT";}
                        else if(e.getValue() == GamepadInputButton.DPAD_DOWNRIGHT_BTN)
                        {dpadPressed ="DPAD DOWN-RIGTH";}
                        else
                        {
                            dpadPressed ="DPAD RELEASED";
                        }
                         
                    
                    }
                 
                 
                 
                 
            });//
                
            step++;
        }//

        @Override
        public void preRender( GraphicsContext g ) 
        {
            drawBackground(g);
          
            drawForeground(g);
            
            drawHUD(g);
   
        }//

    @Override
    public void drawBackground(GraphicsContext g) 
    {
        
        //g.clearRect(0,0, 10*100,levelHeight);
       
        g.setFill(Color.BLACK);
        g.fillRect( 0, 0,  levelWidth, levelHeight);
        
        bg[ 0 ].draw(g);
        bg[ 1 ].draw(g);
        bg[ 2 ].draw(g);
        bg[ 3 ].draw(g);
        bg[ 4 ].draw(g);
        bg[ 5 ].draw(g);
        
        
    }

    @Override
    public void drawForeground(GraphicsContext g) 
    {
            
        g.setFill( Color.WHITE );
        g.fillText( "Jinput Gamepad Example" , 20, 30 );
        
        
        for( int i =0 ; i < button.length ; i++)
        {
        button[ i ].draw( g );
        }
        
        g.setFill( Color.RED );
        
        g.fillRect( xAxisCursor.getX() + xMovCursor , xAxisCursor.getY(), 10, 10);
        g.fillRect( yAxisCursor.getX() + yMovCursor, yAxisCursor.getY(), 10, 10);
        g.fillRect( zAxisCursor.getX() + zMovCursor, zAxisCursor.getY(), 10, 10);
        
        g.fillRect( xRotationCursor.getX()+xRotMovCursor , xRotationCursor.getY(), 10, 10);
        g.fillRect( yRotationCursor.getX()+yRotMovCursor , yRotationCursor.getY(), 10, 10);
        g.fillRect( zRotationCursor.getX()+zRotMovCursor , zRotationCursor.getY(), 10, 10);
        
        
        
        
        
        
        
    }

    @Override
    public void drawHUD(GraphicsContext g) 
    {
        g.setFill( Color.WHITE );
        
        g.fillText( "AXIS" , 50 , 300 );
        g.fillText( "ROTATION" , 120 , 300 );
        
        g.fillText( "X" , 10 , 70 );
        g.fillText( "Y" , 10 , 130 );
        g.fillText( "Z" , 10 , 210 );
        
        g.setFill( Color.GREEN );
        g.fillText( "DPAD: "+dpadPressed , 240 , 300 );
        
    }

     @Override
    public void postRender(GraphicsContext g) 
    {
    }
    
  }//class
 