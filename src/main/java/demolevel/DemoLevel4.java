
package demolevel;

import com.bitless.graphicentity.Background;
import com.bitless.graphicentity.Sprite;
import com.bitless.level.BaseLevel;
import com.bitless.ntfc.LevelDrawable;
import com.bitless.util.ImageUtil;
import javafx.scene.canvas.GraphicsContext;

/**
 *this level will show how to setup a background image
 * @author PavulZavala
 */
public class DemoLevel4 extends BaseLevel
                        implements LevelDrawable
{
    
    Background bg;
    
    Sprite singleFrame, multiFrame;
    
    

    public DemoLevel4(int levelWidth, int levelHeight) 
    {
        super(levelWidth, levelHeight);
    
        bg =  new Background( ImageUtil.getImage( "bgimage.png" ) );
        
    }//

    @Override
    public void preRender(GraphicsContext g)
    {
        drawBackground( g );
    }

    @Override
    public void update(float l) {
    }

    @Override
    public void drawBackground(GraphicsContext g) 
    {
        bg.draw(g);
    }

    @Override
    public void drawForeground(GraphicsContext g) {
    }

    @Override
    public void drawHUD(GraphicsContext g) {
    }
   
     @Override
    public void postRender(GraphicsContext g) 
    {
    }
    
}//c;ass
