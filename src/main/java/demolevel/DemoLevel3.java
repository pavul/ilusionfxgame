package demolevel;


import com.bitless.ntfc.LevelDrawable;
import com.bitless.level.BaseLevel;
import com.bitless.graphicentity.Sprite;
import com.bitless.graphicentity.TextSprite;
import com.bitless.ntfc.Initiable;
import com.bitless.util.ImageUtil;
import javafx.scene.canvas.GraphicsContext;

    /**
     * in this level we create sprites and 
     * check collision between them, also text 
     * is display using default font,
     * it also set implementation for key listeners
     * so the sprite can be controlable thru keyboad
     */
  public  class DemoLevel3 extends BaseLevel
                        implements LevelDrawable, Initiable
    {
        
      Sprite circle;
      
        
      TextSprite msg;
        
        public DemoLevel3(int levelWidth, int levelHeight) 
        {
            super(levelWidth, levelHeight);
         
            initData();
            initImages();
            initBackground();
            initForeground();
            initSprite();
            initHUD();
                 
        }//

        @Override
        public void update( float l ) 
        {
               
            
          
            
        }//

        @Override
        public void preRender( GraphicsContext g ) 
        {
            
            drawBackground(g);
          
            drawForeground(g);
            
            drawHUD(g);
   
        }//

    @Override
    public void drawBackground(GraphicsContext g) 
    {
        
        g.clearRect(0,0, 10*100,levelHeight);
        
    }

    @Override
    public void drawForeground(GraphicsContext g) 
    {
          
        circle.draw( g );
            
    }

    @Override
    public void drawHUD(GraphicsContext g) 
    {
     
        msg.draw( g );
        
    }



    @Override
    public void initData() 
    {
    }

    @Override
    public void initSound() {
    }

    @Override
    public void initImages() 
    {
        imagesList.put( "circle" , ImageUtil.getImageFrames( 1, 32, 32, "circle.png" ) );
    }

    @Override
    public void initSprite()
    {
        
        circle = new Sprite( imagesList.get( "circle" )[ 0 ] );
       
        spriteList.add( circle );
        
        System.err.println("::::SIZE: "+spriteList.size());
        
    }

    @Override
    public void initBackground() 
    {
    }

    @Override
    public void initForeground() 
    {
    }

    @Override
    public void initHUD() 
    {
         msg = new TextSprite(40, 40, "HELLO ILUSION FX");
    }
    
 @Override
    public void postRender(GraphicsContext g) 
    {
    }

    
    }//
    
    
    
    