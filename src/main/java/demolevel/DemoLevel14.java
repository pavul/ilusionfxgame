package demolevel;

import com.bitless.gamedata.GameData;
import com.bitless.ntfc.LevelDrawable;
import com.bitless.level.BaseLevel;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;

/**
 * DEMO for GAMEDATA properties files to
 * load/save values
 */
  public  class DemoLevel14 extends BaseLevel
                            implements LevelDrawable
    {
  
      int x = 0, y = 0, z = 0;
      
      String HUDText ="";
        
        public DemoLevel14(int levelWidth, int levelHeight) 
        {
            super(levelWidth, levelHeight);

       
              System.err.println("::: cargando valores al iniciar ");
              x = Integer.parseInt( GameData.getData( "x" ) );
              y = Integer.parseInt( GameData.getData( "y" ) );
              z = Integer.parseInt( GameData.getData( "z" ) );
              
          
              System.err.println(":::Val "+x+" "+y+" "+z);
          
            
            keyReleasedControl = e->
            {
                 
                if( e.getCode().equals( KeyCode.X ))
                {
                    GameData.setData( "x" , ++x+"", true );
                }
                if( e.getCode().equals( KeyCode.Y ))
                {
                 GameData.setData( "y" , ++y+"", true );
                }
                if( e.getCode().equals( KeyCode.Z ))
                {
                 GameData.setData( "z" , ++z+"", true );
                }
                
                if( e.getCode().equals( KeyCode.BACK_SPACE ))
                {
                    GameData.clearData();
                    System.err.println(":::loading values");
                    try 
                    {
                        GameData.loadData();
                    } 
                    catch (IOException ex) 
                    {
                        Logger.getLogger(DemoLevel14.class.getName()).log(Level.SEVERE, ":::LOAD DATA", ex);
                    }
                    System.err.println(GameData.getDataList());
                    HUDText = "Datos Cargados!";
                }
                if( e.getCode().equals(  KeyCode.ENTER ))
                {
                 System.err.println(":::Saving values");
                    try {
                        GameData.saveData();
                    } 
                    catch (IOException ex) 
                    {
                        Logger.getLogger(DemoLevel14.class.getName()).log(Level.SEVERE, ":::SAVE DATA", ex);
                    }
                    HUDText = "Datos Guardados";
                }
            
            };
            
        }//
        

        @Override
        public void update( float l ) 
        {
            
            
        }//

    @Override
    public void preRender( GraphicsContext g ) 
    {
        drawBackground(g);

        drawForeground(g);

        drawHUD(g);
      
    }//
        
    @Override
    public void postRender(GraphicsContext g) 
    {
    }

    @Override
    public void drawBackground(GraphicsContext g) 
    {
        
        g.setFill( Color.BLACK );
        g.fillRect( 0, 0,  levelWidth, levelHeight);
        
        g.setFill(Color.WHITE);
        g.fillText( "X:  "+x , 100,40);
        g.fillText( "Y: " +y, 200,40);
        g.fillText( "Z: "+z , 300,40);
        
        g.fillText( HUDText , 300,40);
       
    }

    @Override
    public void drawForeground(GraphicsContext g) 
    {
        
    }

    @Override
    public void drawHUD(GraphicsContext g) 
    {
      
    }

     
    
  }//class
 