package demolevel;

import com.bitless.graphicentity.Background;
import com.bitless.graphicentity.Sprite;
import com.bitless.level.BaseLevel;
import com.bitless.ntfc.LevelDrawable;
import com.bitless.util.CollisionUtil;
import com.bitless.util.ImageUtil;
import com.bitless.util.MathUtil;
import javafx.scene.image.Image;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;

/**
 * this level is a game where you control a bucket to catch rain drops This
 * level uses sprites, movement control and collisions It has a HUD that
 * displays how many drops have you catched As well it shows your remain lives
 * and the actual level
 *
 * @author PavulZavala
 * @author Victor Orozco
 * @author Aura Leyva
 * @author Francisco Arellano
 * @author Carlos Ramirez
 * @author Pedro Balleza
 */
public class DemoLevel5 extends BaseLevel
                        implements LevelDrawable 
{

    //Calling images for every sprite in the scene
    Image bucket = ImageUtil.getImage("bucket.png");
    Image drop = ImageUtil.getImage("drop.png");
    //Sprite variables
    Sprite bucketSprite;
    Sprite dropSprite;

    Background bg;
    Sprite singleFrame, multiFrame;
    double xTrans = 0;
    boolean mvRight;
    boolean mvLeft;
    byte spdDrop = 3;
    byte spdBucket = 7;
    byte levelUpCount = 0;
    byte counter = 0;
    byte level = 1;
    byte lives = 3;
    short pause = 0;

    /**
     * Call the level constructor with Width and Height parameters Initialize
     * the Bucket and Drop Sprites Verify the state of keys pressed
     *
     * @param levelWidth
     * @param levelHeight
     */
    public DemoLevel5(int levelWidth, int levelHeight) {
        super(levelWidth, levelHeight);

        bg = new Background(ImageUtil.getImage("bgimage.png"));
        bucketSprite = new Sprite(bucket);
        bucketSprite.setPosition(276, 350);
        dropSprite = new Sprite(drop);
        //Set a random X and Y position to dropSprite 
        dropSprite.setPosition(MathUtil.getRandomRange(0, 640 - dropSprite.getW()),
                                    MathUtil.getRandomRange(0, 100 - dropSprite.getW()));

        //Control the pressed key
        keyPressedControl
                = e -> {

                    System.err.println("key pressed at: " + e.getCode());

                    if (e.getCode().equals(KeyCode.A)) {
                        mvLeft = true;
                    }

                    if (e.getCode().equals(KeyCode.D)) {
                        mvRight = true;
                    }

                };
        //Control the released key
        keyReleasedControl
                = e -> {

                    if (e.getCode().equals(KeyCode.A)) {
                        mvLeft = false;
                    }

                    if (e.getCode().equals(KeyCode.D)) {
                        mvRight = false;
                    }
                };

    }//

    @Override
    /**
     * Call the methods to draw a background, sprites(foreground) and HUD Also
     * draws a GAME OVER HUD when lifes reaches -1
     */
    public void preRender(GraphicsContext g) {
        drawBackground(g);
        drawForeground(g);
        drawHUD(g);
        if (lives < 0) {
            lives = 0;
            drawGameOver(g);
            gm.loadLevel(new LevelMenu(640, 480));

        }
    }

    @Override
    /**
     * Handle all the events during the game execution
     */
    public void update(float l) {

        //Updates the position of dropSprite every frame
        dropSprite.setY(dropSprite.getY() + spdDrop);

        //Next two ifs control the movement and space limits of bucketSprite
        if (mvRight) {
            if (bucketSprite.getX() <= 640 - bucketSprite.getW()) {
                bucketSprite.moveX(spdBucket);
            }
        }
        if (mvLeft) {
            if (bucketSprite.getX() >= 0) {
                bucketSprite.moveX(-spdBucket);
            }
        }
        //

        //Handle collisions betwen bucket and drops
        //Increase the points of catched drops and level difficulty
        if (CollisionUtil.rectangleColision(bucketSprite, dropSprite)) {
            dropSprite.setPosition(MathUtil.getRandomRange(0, 640 - dropSprite.getW()), 100);
            levelUpCount++;
            counter++;
            //If you catch 10 drops then increase the level difficulty
            if (levelUpCount == 10) {
                levelUpCount = 0;
                spdBucket++;
                spdDrop++;
                level++;
            }
            //If you catch 100 drops then you gain an extra life  
            if (counter == 100) {
                counter = 0;
                lives++;
            }
        }
        //The drop relocated when it reaches a 450 Y position 
        //If the drop reaches the 450 Y position you'll lose a life
        if (dropSprite.getY() > 450) {
            dropSprite.setPosition(MathUtil.getRandomRange(0, 640 - dropSprite.getW()), 100);
            lives--;
        }

    }

    @Override
    public void drawBackground(GraphicsContext g) {

        bg.draw(g);
    }

    @Override
    public void drawForeground(GraphicsContext g) {
        try {
            bucketSprite.draw(g);
            dropSprite.draw(g);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    @Override
    public void drawHUD(GraphicsContext g) {
        g.setFill(Color.BLACK);
        g.fillText("Level: " + level, 500, 50);
        g.fillText("Drops: " + counter, 500, 70);
        if (lives < 0) {
            g.fillText("Lives: " + 0, 500, 90);
        } else {
            g.fillText("Lives: " + lives, 500, 90);
        }
    }

    public void drawGameOver(GraphicsContext g) {
        g.setFill(Color.RED);
        g.fillText("GAME OVER", 
                super.camera.getViewWidth() / 2.4,
                super.camera.getViewHeight() / 2 );
    }

     @Override
    public void postRender(GraphicsContext g) 
    {
    }

}//c;ass

