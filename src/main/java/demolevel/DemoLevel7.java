package demolevel;


import com.bitless.graphicentity.LineSprite;
import com.bitless.ntfc.LevelDrawable;
import com.bitless.level.BaseLevel;
import com.bitless.graphicentity.Sprite;
import com.bitless.util.CollisionUtil;
import com.bitless.util.ImageUtil;
import com.bitless.util.MathUtil;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;

    /**
     * this class is used to make testing with circular sprites
     * and circular collisions
     * it also implements and test mouse evens, clicked, pressed, released and moved
     * examples of leghtdir functions implementation
     */
  public  class DemoLevel7 extends BaseLevel
                           implements LevelDrawable
    {
        
     Image[] circleImg; 
     
     Sprite red, blue, yellow;
     Sprite arrow;
     
     LineSprite line;
     
     int lineLength = 300;
     
     //variable to rotate arrow
     int rotation = 0;
     
     
     int spd = 3;
        
        public DemoLevel7(int levelWidth, int levelHeight) 
        {
            super(levelWidth, levelHeight);
        
         circleImg =  ImageUtil.getImageFrames( 3,32,32,"circles.png" );
        
         red = new Sprite( circleImg[0] );
         blue = new Sprite( circleImg[1] );
         yellow = new Sprite( ImageUtil.getImage( "square.png" ) );
         arrow = new Sprite(ImageUtil.getImage( "arrow.png" ) );
         
         
         spriteList.add(red);
         spriteList.add(blue);
         spriteList.add(yellow);
         spriteList.add(arrow);
         
        red.setPosition(100, 100);
        
        blue.setPosition(200, 200);
        
        yellow.setPosition(300, 300);
        
        arrow.setPosition(levelWidth/2, levelHeight/2);
        
        line = new LineSprite(100, 100, 300, 200);
        line.setLineWidth(2);
        line.setLineColor( Color.DARKGREEN );
     
        
        
        keyPressedControl =
                e->{
                    
                        System.err.println("key pressed at: "+e.getCode());

                        if( e.getCode().equals(  KeyCode.A ))
                        {
                            blue.setX( blue.getX()-spd);
                        }

                        if( e.getCode().equals(  KeyCode.D ))
                        {
                            blue.setX( blue.getX()+spd);
                        }
                        
                        if( e.getCode().equals(  KeyCode.W ))
                        {
                            blue.setY( blue.getY()-spd);
                        }

                        if( e.getCode().equals(  KeyCode.S ))
                        {
                           blue.setY( blue.getY()+spd);
                        }
                    
                   };
         
            //events implementation for mouse control
            mouseReleasedControl = e->
            {
            //e.getEventType()
            System.err.println( "released: "+e.getButton().name() );
            System.err.println( "releasedX: "+e.getX() );
            System.err.println( "releasedY: "+e.getY() );
            };
            mousePressedControl = e->
            {
            //e.getEventType()
            System.err.println( "Pressed: "+e.getButton().name() );
            System.err.println( "PressedX: "+e.getX() );
            System.err.println( "PressedY: "+e.getY() );
            };
            mouseClickedControl = e->
            {
            //e.getEventType()
            System.err.println( "Clicked: "+e.getButton().name() );
            System.err.println( "ClickedX: "+e.getX() );
            System.err.println( "ClickedY: "+e.getY() );
            };
            mouseMovedControl = e->
            {
            //e.getEventType()
            System.err.println( "Moved: "+e.getButton().name() );
            System.err.println( "rMovedX: "+e.getX() );
            System.err.println( "MovedY: "+e.getY() );
            };
             
        }//

        @Override
        public void update( float l ) 
        {
               
          rotation+=1;
          arrow.setAngle( rotation );
            
        }//update

        
        @Override
        public void preRender( GraphicsContext g ) 
        {
            
            drawBackground(g);
          
            drawForeground(g);
            
            drawHUD(g);
   
        }//

    @Override
    public void drawBackground(GraphicsContext g) 
    {
        g.clearRect( 0,0, levelWidth,levelHeight );
    }

    @Override
    public void drawForeground(GraphicsContext g) 
    {
          
       spriteList.forEach( spr ->
       {
       spr.draw( g );
       });
       
       drawLaser( g );
       
    }

    @Override
    public void drawHUD(GraphicsContext g) 
    {
        g.setFill(Color.BLACK);
        g.fillText("Circle Collision Demo",20,20);
    }
        


public void drawLaser( GraphicsContext g )
{

    
    g.setStroke(Color.RED);
       g.setLineWidth(2);
       
//       300+16 =316
       float xpos = arrow.getX( );// + arrow.getW()/2;
       float ypos = arrow.getY( );// + arrow.getY()/2;
       
       float xxx = 0 , yyy = 0;
       int i = 0;
       for(i = 1; i < lineLength
               && !CollisionUtil.pointCollision( xxx, yyy, yellow ); i+=1 )
       {
           xxx = xpos + MathUtil.lengthdirX( i , arrow.getAngle() );
           yyy = ypos + MathUtil.lengthdirY( i , arrow.getAngle() );
           
       }
       
       g.beginPath();
       g.moveTo(  arrow.getX()+arrow.getW()/2, arrow.getY()+arrow.getH()/2 );
       g.lineTo( (arrow.getX()) + MathUtil.lengthdirX(i, arrow.getAngle()) ,
                 (arrow.getY()) + MathUtil.lengthdirY(i, arrow.getAngle()));
       g.stroke();
    
}//

 @Override
    public void postRender(GraphicsContext g) 
    {
    }
    
    
    
    
    }//
