package demolevel;


import com.bitless.cfg.Config;
import com.bitless.cfg.GameState;
import com.bitless.graphicentity.Sprite;
import com.bitless.graphicentity.Tile;
import com.bitless.input.GamePad;
import com.bitless.input.GamepadInputButton;
import com.bitless.level.BaseLevel;
import com.bitless.manager.GameManager;
import com.bitless.ntfc.Gamepadable;
import com.bitless.ntfc.Initiable;
import com.bitless.ntfc.LevelDrawable;
import com.bitless.util.CollisionUtil;
import com.bitless.util.GameUtil;
import com.bitless.util.ImageUtil;
import com.bitless.util.SpritetUtil;
import com.bitless.util.TileUtil;
import java.util.List;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

    /**
     * this demo is an example of a game using a camera
     * a level bigger than we can see on screen, it also have 
     * sprites with animations when is idle or waling, hen 
     * jump can be possible and add gravity to walk on top of the solid tiles.
     * this level also have two tilemaps:
     * 1.- to display the tiles according to the level
     * 2.- collision tile to specify which of them are solid tiles
     * which the player can collide.
     * * i display an image or text on the viewX position that way if we
     * are moving along the level, text or images will remain in the same position
     * * it test the distance between player and point in x=200 and y=200, returning
     * distance value
     * 
     */
  public  class DemoLevel8 extends BaseLevel
            implements LevelDrawable, Initiable, Gamepadable
    {
        
        
        Sprite player; //= new Sprite( frames[ 0 ] );
        
        Sprite enemy; // = new Sprite( frames[ 0 ]  );
        
        Sprite []coins;
        
        boolean moveRight;
        boolean moveLeft;
        boolean moveUp;
        boolean moveDown;
        
        int spd = 2;
        int coin = 0;
       
        final byte COLS = 40;
        final byte ROWS = 15;
        
    byte spriteAnim = 0; 
    //0 means iddle
    //1 walking
    

    List<Tile> tileList;
    List<Tile> colTileList;
    
    float grav = 2.5f;//NOFUNCA-3f;// NOFUNCIONA-4f;// 0.98f;
    boolean jump = true;    
    
    GamePad gamePad;
        
    
    
        public DemoLevel8(int levelWidth, int levelHeight, int viewWidth, int viewHeight) 
        {
            super(levelWidth, levelHeight,viewWidth,viewHeight );
         
            /**
             * set all image resources
             */
            initImages();
            
            /**
             * set all backgrounds used
             * in this level
             */
            initBackground();
            
            /**
             * set all sprites
             */
            initSprite();
            
            
              
            Font font =  GameUtil.getFont( Config.FONT_PATH , 6 );
            GameManager.PRE_RENDERER.getGraphicsContext2D().setFont( font );
            
            
            
            //player creation
//         player = new Sprite( frames[ 0 ] );
//         player.setPosition( 20, 20 );
//         //player gonna move to 45 degress direction (clockwise)
         //but that angle wont be set to player angle variable
//         SpritetUtil.moveToAngle( player, 90, spd, false );
         
         
         //enemy sprite is created to have collisions with player, 
//         this will be pushed by the player when colliding
//         enemy = new Sprite( frames[ 1 ]  );
//         enemy.setPosition( 100, 100 );
//         enemy.setLabel( "enemy" );
//         
        
         
//         input controls
         keyPressedControl =
                e->{
                    
                        System.err.println("key pressed at: "+e.getCode());

                        if( e.getCode().equals(  KeyCode.A ) )
                        {
                            moveLeft = true;
                            if(spriteAnim != 1)
                            {
                                spriteAnim = 1;
//                            player.setFrames( SpritetUtil.getAnimationList().get( "playerWalk" ));
                            player.setxScale( -1 );
                            }
                        }

                        if( e.getCode().equals(  KeyCode.D ) )
                        {
                            moveRight = true;
                            if(spriteAnim != 1)
                            {
                                spriteAnim = 1;
//                            player.setFrames( SpritetUtil.getAnimationList().get( "playerWalk" ));
                            player.setxScale( 1 );
                            }
                            
                            
                        }
                        
                        if( e.getCode().equals(  KeyCode.W ))
                        {
                            moveUp = true;
                        }

                        if( e.getCode().equals(  KeyCode.S ))
                        {
                            moveDown = true;
                        }
                    
                   };
         
         
         keyReleasedControl = 
                 e->{  
                 
                  if( e.getCode().equals(  KeyCode.A ))
                    {
                        moveLeft = false;
                         if(spriteAnim != 0)
                           {
                               spriteAnim = 0;
//                            player.setFrames( SpritetUtil.getAnimationList().get( "playerIddle" ));
                           }
                    }
                 
                    if( e.getCode().equals(  KeyCode.D ))
                    {
                        moveRight = false;
                         if(spriteAnim != 0)
                           {
                               spriteAnim = 0;
                           }
                    }   //
                    
                     
                        if( e.getCode().equals(  KeyCode.W ))
                        {
                            moveUp = false;
                        }

                        if( e.getCode().equals(  KeyCode.S ))
                        {
                            moveDown = false;
                        }
                        
                        if( e.getCode().equals(  KeyCode.SPACE ))
                        {
                            if( !jump )
                            {
                            jump = true;
                            grav = -4;
                            }
                            
                        }
                        
                         if( e.getCode().equals(  KeyCode.X ))
                        {
                          ImageUtil.takeSnapshot( gm, levelWidth, levelHeight);
                        }
                         
                         if( e.getCode().equals(  KeyCode.ENTER ) )
                        {
                            if( this.gameState == GameState.PLAYING )
                            {
                                this.gameState = GameState.PAUSED;
                            }
                            else if( this.gameState == GameState.PAUSED )
                            {
                                this.gameState = GameState.PLAYING;
                            }
                          
                        }
                       
                       
                         /**
                          * when Z released distance between player and 200, 200 point is
                          * displayed on console
                          */
                        if( e.getCode().equals(  KeyCode.Z ))
                        {
                            System.err.println("Distance: "+ SpritetUtil.getDistance( player, 200,200));
                         }
                
                 };
     
       this.gameState = GameState.PLAYING;
       
       
       gamePad = new GamePad();
         
        }//

        @Override
        public void update( float l ) 
        {
            
            /**
             * switch to handle different game states
             */
            switch( this.gameState )
            {
                case LOADING:
                    break;
                case PLAYING:

                        
                    
                    
                    //processGamepadController();
                         gamePad.processGamepadInput(e->
                    {
                        
//                        if( e.getName().equals( "X Axis" ))
//                        { System.err.println( "X axis: "+e.getValue() );} 
                        //USING X AXIS
                        //move right
                        
                        
                        if( gamePad.getXAxis(e) == 1f  )
                        {
                            System.err.println( "X AXIS RIGHT: "+e.getValue() );
                            moveRight = true;
                        }
                        //move left
                        else if( gamePad.getXAxis(e) == -1f )
                        {
                            System.err.println( "X AXIS LEFT: "+e.getValue() );
                            moveLeft = true;
                        }
//                        System.err.println( "_____________________________" );
                    
                        if( gamePad.getXAxis(e) == GamepadInputButton.EMPTY_VALUE )
                        {
                             System.err.println( "X AXIS STOP: "+e.getValue() );
                            moveLeft = false;
                            moveRight = false;
                        }
                        
                        
                        
                        //USING DPAD
//                        if( gamePad.isDPadRightPressed(e) )
//                        { moveRight = true; }
//                        
//                        if( gamePad.isDPadLeftPressed(e) )
//                        { moveLeft = true; }
//                        
//                        if( gamePad.isDPadReleased(e) )
//                        {
//                        moveLeft = false;
//                        moveRight =  false;
//                        }
                        
                        
                    });
                    
                    
                    
                         /**
                         * used to move the camera view on X axis 0.5 pixels every time
                         */
                        camera.moveX( 0.5f );
                        //camera.moveY( 0.1f );


                        //movement of player
                        if(moveRight)  player.moveX( spd );
                        if(moveLeft)  player.moveX( -spd );
                        if(moveUp)  player.moveY( -spd );
                        if(moveDown)  player.moveY( spd );


                        /**
                         * update player animation
                         */
                        player.update( l );

                        

                        

                        //while player is jumping we will dcrease jump force
                        //to make the sprite fall
                        if( jump )
                        {
                            grav += 0.3;
                            if( grav >= 4)grav = 4;
                        }


                        //if player is not jumping then we will apply
                        //some minor gravity to fall from tiles even if
                        //is not jumping
                        player.moveY( grav );


                        /**
                         * checking collision between player and tiles at the bottom, to stop
                         * gravity
                         */
                        colTileList.forEach( t->
                        {
                            if( CollisionUtil.rectangleColision( player, t, false ).equals( "BOTTOM" )
                                    && grav >= 0 )
                                {
                                   jump = false;
                                   grav = 2;
                                }
                        });


                    //collision with tiles
                    for( byte i = 0; i < 9 ; i++)
                    {
                        if (CollisionUtil.rectangleColision( player, coins[ i ] ) && coins[ i ].isVisible() )
                        {
                        coins[ i ].setVisible( false );
                        coin ++;
                        }
                    }//
                    
                     
                    break;
                case PAUSED:
                    break;
                case GAMEOVER:
                    break;
            }//suich
            
        }//update

        @Override
        public void preRender( GraphicsContext g ) 
        {
            
           //camera draw
            g.save();
            g.translate( camera.getCamx(), camera.getCamy() );
            
            drawBackground(g);
          
            drawForeground(g);
            
            drawHUD(g);
            
            g.restore();
            
           
        }//

        @Override
    public void postRender( GraphicsContext g ) 
    {
        g.save();
        g.translate( camera.getCamx(), camera.getCamy() );
        g.setFill(Color.ORANGE);
        g.fillRect(camera.getViewX(), camera.getViewY(), 640, 480 );
        g.clearRect( player.getX(), player.getY(), 320, 480 );
        
        g.setFill( Color.RED );
        g.fillText("TEST TEXT", camera.getViewX() + 20, camera.getViewY()+ 20); 
        
        g.restore();
        
        
        
        
    }//
        
        
    @Override
    public void drawBackground(GraphicsContext g) 
    {
        
            g.setFill( Color.SKYBLUE );
            g.fillRect(0,0,levelWidth, levelHeight );
            
            TileUtil.drawTiles(
                    g, 
                    imagesList.get( "bgTiles" ), 
                    tileList );
            
            //_________________
            //uncomment to make appear tile collision boxes
            colTileList.forEach( t->
            {
               g.strokeRect(t.getX(), t.getY(), t.getW(), t.getH());}
            );
           //________________________
    }

    @Override
    public void drawForeground(GraphicsContext g) 
    {
        
        /**
         * draw tiles and player only on PLAYING game state
         */
        switch( this.gameState )
        {
                case LOADING:
                    break;
                case PLAYING:
                    
                    g.setFill( Color.RED );
                    player.draw( g );
                    
                    
                    enemy.draw( g );

                    for( byte i=0; i < 10 ;i++)
                    {
                    coins[i].draw( g );
                    }
           //uncoment to make boxes collision appear on sprites
//           g.strokeRect(player.getX(), player.getY(), player.getW(), player.getH());
//           g.strokeRect(enemy.getX(), enemy.getY(), enemy.getW(), enemy.getH());
                    break;
                case PAUSED:
                    break;
        }//suich
        
  
    }//

    @Override
    public void drawHUD(GraphicsContext g) 
    {
    
        
        switch( this.gameState )
        {
                case LOADING:
                    break;
                case PLAYING:
                    /**
         * this level is using a camera to move the view among the whole
         * level, this means X position is static, to display text on
         * the view
         */
        g.setFill( Color.RED );
        g.fillText("TEST TEXT", camera.getViewX() + 20, camera.getViewY()+ 20);   
        
        g.setFill( Color.BLACK );
        g.fillText("x 2", camera.getViewX() + 40, camera.getViewY() + 36);   
        
        g.setFill( Color.BLACK );
        g.fillText("Coin: "+coin, camera.getViewX() + 80, camera.getViewY() + 36);   
                    break;
                case PAUSED:
        g.setFill( Color.BLACK );
        g.fillText( "Game Paused", camera.getViewX() + (camera.getViewWidth()/2), camera.getViewY()+ (camera.getViewHeight()/2) ); 
        System.err.println("-->"+(camera.getViewY()));
        System.err.println("-->"+(camera.getViewY()+ levelHeight/2));
        break;
        }//
        
        
    }

    @Override
    public void initData() 
    {
        
        
    }//

    @Override
    public void initSound() 
    {
    }//

    @Override
    public void initSprite() 
    {
        player = new Sprite( imagesList.get( "playerIddle" ) );
        player.setPosition(60, 100);
        
        enemy = new Sprite( imagesList.get( "playerIddle" )[0] );
        enemy.setPosition(60, 68);
        
        
        
        /**
         * set the coins
         */
        coins = new Sprite[ 10 ];
        for( byte i=0; i < 10 ;i++)
        {
        coins[i] = new Sprite( imagesList.get( "coinIcon" )[0] );
        }
        
        
        //set the correct position for the coins
        int []coinPlace =
         {
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,
            0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,1,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
         };
        
    List<Tile> lt =   TileUtil.parseTiles( coinPlace, 16, 16,ROWS, COLS  );
    
    for( byte i = 0; i < 9 ;i++ )
    {
        Tile t = lt.get( i );
        coins[ i ].setX( t.getX() );
        coins[ i ].setY( t.getY() );
    }
    
        
    }//

    @Override
    public void initBackground() 
    {
        
        int [] bgTiles = 
        {
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,7,7,7,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0, 0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0, 0,0,1,1,1,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,
            7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7, 7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
            1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
        };
        
        
         int [] colTiles = 
        {
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0, 0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0, 0,0,1,1,1,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,
            1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1, 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
        };
         
         
         
         
         
        //tiles to draw
        tileList =  TileUtil.parseTiles( bgTiles, 16, 16,ROWS, COLS  );
        System.err.println("TILE SIZEE: "+tileList.size());
        
        
        //tiles to collide
        colTileList =  TileUtil.parseTiles( colTiles, 16, 16,ROWS, COLS  );
        System.err.println("COLTILE SIZE: "+colTileList.size());
        
        
        
        
        
        
        
        bgTiles = null;//make disposable
    }//

    @Override
    public void initForeground() 
    {
    }

    @Override
    public void initHUD() {
    }

    
    @Override
    public void initImages() 
    {
        imagesList.put( "bgTiles" , 
                ImageUtil.getImageFrames(20, 16, 16, "terraintiles.png")  );
        
        imagesList.put( "playerIddle" , 
                ImageUtil.getImageFrames(4, 21, 32, "char_idle_anim.png")  );
        
        imagesList.put( "playerWalk" , 
                ImageUtil.getImageFrames(4, 21, 32, "char_walk_anim.png")  );
        
        imagesList.put( "playerLiveIcon" , 
                ImageUtil.getImageFrames(1, 16, 16, "headlife.png")  );

        imagesList.put( "coinIcon" , 
                ImageUtil.getImageFrames(1, 16, 16, "coin.png")  );
        
    }//
        

    /**
     * used to load or execute code that is ready after the level was
     * created and loaded by GameManager, for example change the font
     * 
     */
//    @Override
//    public void onGameloaded() 
//    {
        //we can change the font or font size 
        //after level loads
      
        
        //set blur effect
        //       BoxBlur fx = new BoxBlur( 2, 2, 3);

        //set glow effect
        //       Glow fx = new Glow(1);

        //       set drop shadow effect
        //        DropShadow fx = new DropShadow(3,3,3,Color.BLACK);

        //innershadow effect
        //        InnerShadow fx = new InnerShadow(2, Color.BLACK);


        //reflection effect WORKS WELL
        //       Reflection fx = new Reflection();
        //       fx.setFraction( 0.9 );
       

        //Lighting effect
        //        Distant light = new Distant();
        //        light.setAzimuth(-35.0f);
        // 
        //        Lighting fx = new Lighting();
        //        fx.setLight(light);
        //        fx.setSurfaceScale(2.5f);

        //
        //    Blend fx = new Blend();
        //    fx.setMode( BlendMode.COLOR_BURN );
        //    fx.setMode( BlendMode.ADD );
        //    fx.setMode( BlendMode.BLUE );
        //    fx.setMode( BlendMode.COLOR_DODGE );
        //    fx.setMode( BlendMode.DARKEN );
        //    fx.setMode( BlendMode.DIFFERENCE );
        //    fx.setMode( BlendMode.EXCLUSION );
        //    fx.setMode( BlendMode.GREEN );
        //    fx.setMode( BlendMode.HARD_LIGHT );
        //    fx.setMode( BlendMode.LIGHTEN );
        //    fx.setMode( BlendMode.MULTIPLY );
        //    fx.setMode( BlendMode.OVERLAY );
        //    fx.setMode( BlendMode.SCREEN );
        //    fx.setMode( BlendMode.SOFT_LIGHT );
        //    fx.setMode( BlendMode.SRC_ATOP );
        //    fx.setMode( BlendMode.SRC_OVER );
        //
        //
        //       gm.getG().setEffect( fx );
       
       
       
       
//    }

    @Override
    public void processGamepadController() 
    {
    
         gamePad.processGamepadInput(e->
                    {
                        
//                        if( e.getName().equals( "X Axis" ))
//                        { System.err.println( "X axis: "+e.getValue() );} 
                        //USING X AXIS
                        //move right
                        
                        if( gamePad.getXAxis(e) == GamepadInputButton.EMPTY_VALUE )
                        {
                             System.err.println( "X AXIS STOP: "+e.getValue() );
                            moveLeft = false;
                            moveRight = false;
                        }
                        else if( gamePad.getXAxis(e) == 1f  )
                        {
                            System.err.println( "X AXIS RIGHT: "+e.getValue() );
                            moveRight = true;
                        }
                        //move left
                        else if( gamePad.getXAxis(e) == -1f )
                        {
                            System.err.println( "X AXIS LEFT: "+e.getValue() );
                            moveLeft = true;
                        }
//                        System.err.println( "_____________________________" );
                    
                        
                        
                        
                        
                        //USING DPAD
                        if( gamePad.isDPadRightPressed(e) )
                        { moveRight = true; }
                        
                        if( gamePad.isDPadLeftPressed(e) )
                        { moveLeft = true; }
                        
                        if( gamePad.isDPadReleased(e) )
                        {
                        moveLeft = false;
                        moveRight =  false;
                        }
                        
                        
                    });
        
    
    }
    
     
    
    
    }//