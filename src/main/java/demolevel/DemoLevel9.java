package demolevel;


import com.bitless.ntfc.LevelDrawable;
import com.bitless.level.BaseLevel;
import com.bitless.ntfc.Initiable;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.effect.BlendMode;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;


    /**
     * in this level we play background and sound effect music
     */
  public  class DemoLevel9 extends BaseLevel
        implements LevelDrawable, Initiable
    {
     
      
      private float volume=0;
      private boolean muted;
      private String audioText = "";
      
       
        public DemoLevel9(int levelWidth, int levelHeight) 
        {
            super(levelWidth, levelHeight);
         
            initSound();
            
            keyReleasedControl = e->
            {  
            
                /**
                * use this code to mute the song
                */
                if( e.getCode().equals(  KeyCode.D ))
                {
                muted = !muted;
                audioManager.mute( muted );
                }


                /**
                * use this code to decrease volume
                */
                if( e.getCode().equals(  KeyCode.S ))
                {
                volume -= 0.1;
                audioManager.setMusicVolume( volume );
                }
                
                /**
                 * use this to increate volume
                 */
                if( e.getCode().equals(  KeyCode.W ))
                {
                volume += 0.1;
                audioManager.setMusicVolume( volume );
                }
                
                if( e.getCode().equals(  KeyCode.A ))
                {
                audioManager.playSfx( "audio1" );
                audioText = "Audio Is Playing!";
                }
                
            };//keyreleased
            
            
            
            audioManager.playMusic( "lettucephyllis.mp3" );
            
         }//
        

        @Override
        public void update( float l ) 
        {
               
           if( !audioManager.getSfxList().get( "audio1" ).isPlaying()  )
           {
           audioText = "Audio is not Playing";
           }
           
        }//

        @Override
        public void preRender( GraphicsContext g ) 
        {
//            g.save();
            drawBackground(g);
          
            drawForeground(g);
            
            drawHUD(g);
   
            g.setGlobalBlendMode( BlendMode.SRC_ATOP );
//            g.restore();
        }//

    @Override
    public void drawBackground(GraphicsContext g) 
    {
//        g.clearRect(0,0, levelWidth,levelHeight);
        
        g.setFill(Color.WHITE);
        g.fillRect( 0, 0, levelWidth, levelHeight );
    }

    @Override
    public void drawForeground(GraphicsContext g) 
    {
            
    }

    @Override
    public void drawHUD(GraphicsContext g) 
    {
        g.setFill(Color.BLACK);
        g.fillText(audioText, 40, 40 );
        g.fillText("D = mute/unmute, A = play sfx, W = vol+, S = vol-", 40, 80 );
        g.fillText("volume: "+volume, 40, 120 );
        g.fillText("Music is " +( muted?"muted":"unmuted" )  , 40, 160 );
    }


    @Override
    public void initData() 
    {
    }

    @Override
    public void initSound() 
    {
        
        audioManager.loadSfx( "audio1", "buttonerror.wav" );
        
    }//

    @Override
    public void initImages() {
    }

    @Override
    public void initSprite() {
    }

    @Override
    public void initBackground() {
    }

    @Override
    public void initForeground() {
    }

    @Override
    public void initHUD() {
    }
        
     @Override
    public void postRender(GraphicsContext g) 
    {
    }
        
    }//
    
    
    
    