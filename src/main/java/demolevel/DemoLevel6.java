package demolevel;


import com.bitless.ntfc.LevelDrawable;
import com.bitless.level.BaseLevel;
import com.bitless.graphicentity.Sprite;
import com.bitless.util.CollisionUtil;
import com.bitless.util.ImageUtil;
import com.bitless.util.SpritetUtil;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;

    /**
     * this class is used to make testing with circular sprites
     * and circular collisions
     */
  public  class DemoLevel6 extends BaseLevel
        implements LevelDrawable
    {
        
     Image[] circleImg; 
     
     Sprite red, blue, yellow, blue2;
        
     int spd = 3;
        
        public DemoLevel6(int levelWidth, int levelHeight) 
        {
            super(levelWidth, levelHeight);
         
         circleImg =  ImageUtil.getImageFrames( 3,32,32,"circles.png" );
         
        new Sprite(circleImg[0]).setLabel("red");
        new Sprite(circleImg[1]).setLabel("blue");
        new Sprite(circleImg[1]).setLabel("blue2");
        new Sprite(circleImg[2]).setLabel("yellow");
         
        red = (Sprite) SpritetUtil.getSpritesByLabel(spriteList, "red").get( 0 );
        red.setPosition(100, 100);
        
        blue = (Sprite) SpritetUtil.getSpritesByLabel(spriteList, "blue").get( 0 );
        blue.setPosition(200, 200);
        
        yellow = (Sprite) SpritetUtil.getSpritesByLabel(spriteList, "yellow").get( 0 );
        yellow.setPosition(300, 300);
        
        blue2 = (Sprite) SpritetUtil.getSpritesByLabel(spriteList, "blue2").get( 0 );
        blue2.setPosition(300, 100);
//        
        
     
        
        
        keyPressedControl =
                e->{
                    
                        System.err.println("key pressed at: "+e.getCode());

                        if( e.getCode().equals(  KeyCode.A ))
                        {
                            blue.setX( blue.getX()-spd);
                        }

                        if( e.getCode().equals(  KeyCode.D ))
                        {
                            blue.setX( blue.getX()+spd);
                        }
                        
                        if( e.getCode().equals(  KeyCode.W ))
                        {
                            blue.setY( blue.getY()-spd);
                        }

                        if( e.getCode().equals(  KeyCode.S ))
                        {
                           blue.setY( blue.getY()+spd);
                        }
                    
                   };
         
        
        
        
        }//

        @Override
        public void update( float l ) 
        {
               
            //collision between 2 circles
          if(  CollisionUtil.circleCollision(
                    blue.getX()+blue.getW()/2,
                    blue.getY()+blue.getH()/2,
                    blue.getW()/2,
                    red.getX()+red.getW()/2,
                    red.getY()+red.getH()/2,
                    red.getW()/2) )
          {
              System.err.println("COLLISION TRUE WITH RED & BLUE");
          }
           
          //collision between sprite and circle, if overlap is true(last param), 
          //means it wont let both circles overlaping each other
          if( CollisionUtil.circleCollision(
                  blue, 
                  yellow.getX()+yellow.getW()/2, 
                  yellow.getY()+yellow.getH()/2, 
                  yellow.getW()/2, true) )
          {
              System.err.println("COLLISION TRUE WITH YELLOW & BLUE");
          }
            
          
        if( CollisionUtil.circleCollision( blue, blue2, true ) )
        {
        System.err.println("COLLISION TRUE WITH Blue circles");
        }
          
          
        }//update

        
        
        
        
        
        @Override
        public void preRender( GraphicsContext g ) 
        {
            
            drawBackground(g);
          
            drawForeground(g);
            
            drawHUD(g);
   
        }//

    @Override
    public void drawBackground(GraphicsContext g) 
    {
        g.clearRect( 0,0, levelWidth,levelHeight );
    }

    @Override
    public void drawForeground(GraphicsContext g) 
    {
          
       spriteList.forEach( spr ->
       {
       spr.draw( g );
       });
            
    }

    @Override
    public void drawHUD(GraphicsContext g) 
    {
        g.setFill(Color.BLACK);
        g.fillText("Circle Collision Demo",20,20);
    }

         @Override
    public void postRender(GraphicsContext g) 
    {
    }
        
    }//
    
    
    
    