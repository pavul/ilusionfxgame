package game.pong;

import com.bitless.graphicentity.Sprite;
import com.bitless.level.BaseLevel;
import com.bitless.ntfc.LevelDrawable;
import com.bitless.util.ImageUtil;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;

/**
 *
 * @author Pedro Balleza
 */
public class Pong extends BaseLevel
        implements LevelDrawable {

    //Sprite Image Reference
    Image player1Img = ImageUtil.getImage("game/pong/Player1.png");
    Image player2Img = ImageUtil.getImage("game/pong/Player2.png");
    Image ballImg = ImageUtil.getImage("bullet.png");
    
    Sprite player; 
    Sprite player2;
    Sprite ball; 
    
    //ball movement variables
    boolean isOnMovement;
    boolean bounceY = false;
    boolean bounceLeft2 =false;
  
    //Player movement
    boolean Player1Up;
    boolean Player1Down;
    boolean Player2Up;
    boolean Player2Down;
    
    // counter HUD variables
    static int countPlayer1= 0;
    static int countPlayer2= 0;
    
    int spdp1 = 4;
    int spdp2 = 4;

    public Pong(int levelWidth, int levelHeight) {
        super(levelWidth, levelHeight);

        
        player = new Sprite(player1Img);
        player.setPosition(10, 240);
        player2 = new Sprite(player2Img);
        player2.setPosition(610, 240);
        
        ball = new Sprite(ballImg);
        ball.setPosition(this.levelWidth / 2, this.levelHeight / 2);
        //ball.setSpdX(spdXBall);
        //ball.setSpdY(spdYBall);

//         input controls
        keyPressedControl
                = e -> {

                    System.err.println("key pressed at: " + e.getCode());

                    if (e.getCode().equals(KeyCode.UP)) {
                        Player2Up = true;
                    }

                    if (e.getCode().equals(KeyCode.DOWN)) {
                        Player2Down = true;
                    }

                    if (e.getCode().equals(KeyCode.W)) {
                        Player1Up = true;
                    }

                    if (e.getCode().equals(KeyCode.S)) {
                        Player1Down = true;
                    }

                };

        keyReleasedControl
                = e -> {

                    if (e.getCode().equals(KeyCode.UP)) {
                        Player2Up = false;
                    }

                    if (e.getCode().equals(KeyCode.DOWN)) {
                        Player2Down = false;
                    }

                    if (e.getCode().equals(KeyCode.W)) {
                        Player1Up = false;
                    }

                    if (e.getCode().equals(KeyCode.S)) {
                        Player1Down = false;
                    }

                    if (e.getCode().equals(KeyCode.SPACE)) {
//                            moveFloor = true;
                        ball.setSpdX(2);
                        ball.setSpdY(-2);
                        isOnMovement = true;
                    }

                };
    }//

    @Override
    public void update(float l) 
    {
        
        System.out.println(ball.getY() +" "+ball.getX());
        //Rebounce on Y limits controlling
        if (ball.getY() <= 0 || ball.getY() >= (levelHeight-ball.getH()))
        {
            if(!bounceY)
            {
                System.out.println("rebote");
                System.out.println(ball.getSpdY());
                ball.setSpdY(ball.getSpdY()*-1); 
                System.out.println(ball.getSpdY());
                bounceY=true;
            }
            
        }
        
        //If the ball cross X limits counter increase +1 the ball will stop
        //position will be the middle of the screen
        if (ball.getX() <= 0 || ball.getX() >= levelWidth-ball.getW() ) 
        {
            
          
         if(ball.getX()<= 0)
         {
             countPlayer2++;
         }
         else
         {
             countPlayer1++;
         }
         
         isOnMovement=false;
         ball.setSpdX( 0 );
         ball.setSpdY( 0 );
         ball.setPosition(this.levelWidth/2, this.levelHeight/2);   
        }
        
        //Player 1 movement and limits
        if (Player1Up)
        {
            if (player.getY() > 0) {
                player.moveY(-spdp1);
            }
        }
        if (Player1Down) 
        {
            if (player.getY() <= 480 - player.getH()) {
                player.moveY(spdp1);
            }
        }
         //Player 2 movement and limits
        if (Player2Up) 
        {
            if (player2.getY() > 0) {
                player2.moveY(-spdp2);
            }
        }
        if (Player2Down) {

            if (player2.getY() <= 480 - player2.getH()) {
                player2.moveY(spdp2);
            }
        }
        //movement of the ball
        if (isOnMovement) 
        {      
            ball.moveXSpd();
            ball.moveYSpd();
            if(ball.getY()>0)
            {
                bounceY=false;
            }
        }
        
        //String s =CollisionUtil.rectangleColision(player, ball, false);
        //String s2= CollisionUtil.rectangleColision(ball, ball, Player1Up)
        
        //Controlling collision player 1
        
        /*
        if (CollisionUtil.rectangleColision(ball, player,false).equals("LEFT")) 
        {
            System.out.println("im left"); 
            ball.setSpdX( ball.getSpdX()*-1 );
            
        }
        if(CollisionUtil.rectangleColision(ball, player,false).equals("RIGHT")) 
                 {
            System.out.println("im right");
            spdXBall*= -1;
            ball.setSpdX( spdXBall );
        }
        if(CollisionUtil.rectangleColision(ball, player,false).equals("TOP"))
                 {
            System.out.println("im top");
            spdYBall*= -1; 
            ball.setSpdY( spdYBall );
        }
        if(CollisionUtil.rectangleColision(ball, player,false).equals("BOTTOM"))
                 {
            System.out.println("im bottom");
            spdYBall*= -1; 
            ball.setSpdY( spdYBall );
        }
        
        
        if(CollisionUtil.rectangleColision(ball, player2,false).equals("BOTTOM"))
        {
            System.out.println("im bottom");
            spdYBall*= -1; 
            ball.setSpdY( spdYBall );
            
        }
        if(CollisionUtil.rectangleColision(ball, player2,false).equals("TOP"))
        {
            System.out.println("im top");
            spdYBall*= -1; 
            ball.setSpdY( spdYBall );
        }
        if(CollisionUtil.rectangleColision(ball, player2,false).equals("LEFT"))
        {
            System.out.println("im left");
            if(!bounceLeft2)
            {
            ball.setSpdX( ball.getSpdX()*-1 );
            bounceLeft2=true;
            }
        }
        if(CollisionUtil.rectangleColision(player2, ball,false).equals("RIGHT"))
        {
            System.out.println("im right");
            spdXBall*= -1;
            ball.setSpdX( spdXBall );   
        }*/
        
        
        

    }//update

    @Override
    public void preRender(GraphicsContext g) {

        drawBackground(g);
        drawForeground(g);
        drawHUD(g);

    }//

    @Override
    public void drawBackground(GraphicsContext g) {

        g.clearRect(0, 0, 10 * 100, levelHeight);
        //g.setFill(Color.BLUE);

    }

    @Override
    public void drawForeground(GraphicsContext g) {
        player.draw(g);
        player2.draw(g);
        ball.draw(g);

    }

    @Override
    public void drawHUD(GraphicsContext g) {

        //Drawing the counters
        g.setFill(Color.RED);
        g.fillText("Player 1: ", 20, 20);
        g.fillText(String.valueOf(countPlayer1), 120, 20);
        g.setFill(Color.BLUE);
        g.fillText("Player 2: ", 360, 20);
        g.fillText(String.valueOf(countPlayer2), 460, 20);
        
    }

     @Override
    public void postRender(GraphicsContext g) 
    {
    }
    
}//
