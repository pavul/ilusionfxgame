/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.tictactoe;

import com.bitless.graphicentity.LineSprite;
import com.bitless.ntfc.LevelDrawable;
import com.bitless.level.BaseLevel;
import com.bitless.util.ImageUtil;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

/**
 * This level is the Tic Tac Toe game using mouse events on the board.
 *
 * @author Carlos
 */
public class Tictactoe extends BaseLevel
        implements LevelDrawable {

    //this list is going to be used to create lines on the board
    List<LineSprite> lines;
    int lineLength = 400;
    //variable to define the area clicked on the board
    byte areaClicked;
    //variable to know the current player
    byte player = 1;
    //variable to define the winner
    byte winner = 0;
    //variable to save the sate of the areas on the board
    byte board[] = {
        0, 0, 0,
        0, 0, 0,
        0, 0, 0
    };
    //variable to see if there is a tie
    byte counter;
    //flag to see if the game is finished
    boolean status;
    Image oImage = ImageUtil.getImage("game/tictactoe/O.png");
    Image xImage = ImageUtil.getImage("game/tictactoe/X.png");

    public Tictactoe(int levelWidth, int levelHeight) {
        super(levelWidth, levelHeight);
        this.status = false;
        this.player = 1;
        this.counter = 0;
        lines = new ArrayList<>();
        //LineSprite ( x1 , y1, x2, y2)
        //horizontal lines properties
        lines.add(new LineSprite(100, 170, 550, 170));
        lines.add(new LineSprite(100, 330, 550, 330));
        //vertical lines properties
        lines.add(new LineSprite(240, 30, 240, 450));
        lines.add(new LineSprite(410, 30, 410, 450));

        for (int i = 0; i <= 3; i++) {
            lines.get(i).setLineWidth(6);
        }

        //handle mouse click
        mouseClickedControl = e
                -> {
            //where is the area clicked
            this.areaClicked = checkAreaClicked(e.getX(), e.getY());

            System.err.println("Clicked: " + e.getButton().name());
            System.err.println("ClickedX: " + e.getX());
            System.err.println("ClickedY: " + e.getY());
            // change the content of the board using te area clicked
            if (handleArea(this.areaClicked)) 
            {
                System.out.println("board " + Arrays.toString(board));
                //check if there is a winner condition on the board
                if (checkBoard(board)) {
                    this.status = true;
                    this.winner = this.player;
                    System.out.println("the winner is Player " + this.player);
                }
                switch (this.player) {
                    case 1:
                        this.player = 2;
                        break;
                    case 2:
                        this.player = 1;
                        break;
                }
            }

        };
    }

    /**
     * this method returns true if there is a winning condition on the board[]
     * checking if there are three places equals on the board
     */
    public boolean checkBoard(byte[] b) {
        if (b[0] == b[1] && b[1] == b[2]) {
            if (b[0] != 0) {
                return true;
            }
        } else if (b[3] == b[4] && b[4] == b[5]) {
            if (b[3] != 0) {
                return true;
            }
        } else if (b[6] == b[7] && b[7] == b[8]) {
            if (b[6] != 0) {
                return true;
            }
        } else if (b[0] == b[3] && b[3] == b[6]) {
            if (b[0] != 0) {
                return true;
            }
        } else if (b[1] == b[4] && b[4] == b[7]) {
            if (b[1] != 0) {
                return true;
            }
        } else if (b[2] == b[5] && b[5] == b[8]) {
            if (b[2] != 0) {
                return true;
            }
        } else if (b[0] == b[4] && b[4] == b[8]) {
            if (b[0] != 0) {
                return true;
            }
        } else if (b[2] == b[4] && b[4] == b[6]) {
            if (b[2] != 0) {
                return true;
            }
        }

        return false;
    }

    

    /**
     * this method checks if the area clicked is already used or can display
     * a the player X or Y, there is a counter that is going to be
     * increased to see if all the board is used and there is a tie.
     * @param a
     * @return 
     */
    public boolean handleArea(byte a) {
        if (a == 9) {
            return false;
        } else if (this.board[a] != 0) {
            return false;
        } else {

            this.board[a] = this.player;
            this.counter += 1;
            return true;
        }
    }
    /**
     * this method checks if the area clicked is valid to draw an X or Y
     * and returns the number of the area to be changed
     * @param x coordinate
     * @param y coordinate
     * @return area clicked
     */
    public byte checkAreaClicked(double x, double y) {
        byte area = 9;
        if (x > 410 && x < 550) {
            if (y > 30 && y < 170) {
                area = 2;
            }
            if (y > 171 && y < 330) {
                area = 5;
            }
            if (y > 331 && y < 450) {
                area = 8;
            }
        } else if (x > 240 && x < 409) {
            if (y > 30 && y < 170) {
                area = 1;
            }
            if (y > 171 && y < 330) {
                area = 4;
            }
            if (y > 331 && y < 450) {
                area = 7;
            }
        } else if (x > 100 && x < 239) {
            if (y > 30 && y < 170) {
                area = 0;
            }
            if (y > 171 && y < 330) {
                area = 3;
            }
            if (y > 331 && y < 450) {
                area = 6;
            }
        } else {
            area = 9;
        }
        System.out.println("clicked area " + area);
        return area;
    }
    /**
     * this method draws Xs and Os on the board using the board list
     * @param g 
     */
    @Override
    public void drawBackground(GraphicsContext g) {
        g.clearRect(0, 0, levelWidth, levelHeight);
        for (int i = 0; i < this.board.length; i++) {
            if (this.board[i] != 0) {
                if (this.board[i] == 1) {
                    switch (i) {
                        case 0:
                            g.drawImage(xImage, 85, 25);
                            break;
                        case 1:
                            g.drawImage(xImage, 240, 25);
                            break;
                        case 2:
                            g.drawImage(xImage, 410, 25);
                            break;
                        case 3:
                            g.drawImage(xImage, 85, 150);
                            break;
                        case 4:
                            g.drawImage(xImage, 240, 150);
                            break;
                        case 5:
                            g.drawImage(xImage, 410, 150);
                            break;
                        case 6:
                            g.drawImage(xImage, 85, 310);
                            break;
                        case 7:
                            g.drawImage(xImage, 240, 310);
                            break;
                        case 8:
                            g.drawImage(xImage, 410, 310);
                            break;
                    }
                } else if (this.board[i] == 2) {
                    switch (i) {
                        case 0:
                            g.drawImage(oImage, 85, 25);
                            break;
                        case 1:
                            g.drawImage(oImage, 240, 25);
                            break;
                        case 2:
                            g.drawImage(oImage, 410, 25);
                            break;
                        case 3:
                            g.drawImage(oImage, 85, 150);
                            break;
                        case 4:
                            g.drawImage(oImage, 240, 150);
                            break;
                        case 5:
                            g.drawImage(oImage, 410, 150);
                            break;
                        case 6:
                            g.drawImage(oImage, 85, 310);
                            break;
                        case 7:
                            g.drawImage(oImage, 240, 310);
                            break;
                        case 8:
                            g.drawImage(oImage, 410, 310);
                            break;
                    }
                }
            }
        }
    }

    @Override
    public void drawForeground(GraphicsContext g) {
        for (int i = 0; i <= 3; i++) {
            lines.get(i).draw(g);
        }
    }

    @Override
    public void drawHUD(GraphicsContext g) {

    }

    @Override
    public void preRender(GraphicsContext g) {
        drawBackground(g);
        //check if the winner condition is true or there is a tie and end the game
        if (this.status == true) {
            drawGameOver(g, 0);
        } else if (this.counter == 9) {
            drawGameOver(g, 1);
        }
        drawForeground(g);
    }

    @Override
    public void update(float l) {
        //if(){

        //}
    }
    //this method draws a Game Over 
    public void drawGameOver(GraphicsContext g, int t) {
        g.setFill(Color.RED);
        switch (t) {
            case 0:
                g.fillText("THE WINNER IS THE PLAYER : " + this.winner,
                        levelWidth / 2.4,
                        levelHeight / 2);
                break;
            case 1:
                g.fillText("IT IS A TIE",
                        levelWidth / 2.4,
                        levelHeight / 2);
                break;
        }
}

     @Override
    public void postRender(GraphicsContext g) 
    {
    }
    
}
