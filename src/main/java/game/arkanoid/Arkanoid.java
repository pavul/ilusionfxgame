/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game.arkanoid;

import demolevel.LevelMenu;
import com.bitless.graphicentity.Background;
import com.bitless.graphicentity.Sprite;
import com.bitless.level.BaseLevel;
import com.bitless.ntfc.LevelDrawable;
import com.bitless.util.CollisionUtil;
import com.bitless.util.ImageUtil;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.image.Image;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;

/**
 * this level is the classic game knowed as Arkanoid or Bricks
 * You have to destroy bricks using a ball and a bar to prevent that the ball falls. 
 * This level uses sprites, movement control and collisions. It has a HUD that displays how
 * many lives do you have, actual level and destroyed bricks
 *
 * @author PavulZavala
 * @author Victor Orozco
 */
public class Arkanoid extends BaseLevel
        implements LevelDrawable {

    //Calling images for every sprite in the scene
    Image barImg = ImageUtil.getImage("game/arkanoid/bar.png");
    Image backgroundImg = ImageUtil.getImage("game/arkanoid/background.png");
    Image ballImg = ImageUtil.getImage("game/arkanoid/ball.png");
    Image[] bricksImg = ImageUtil.getImageFrames(3, 80, 29, "game/arkanoid/bricks.png");
    //Sprite variables
    Sprite barSprite;
    Sprite ballSprite;
    Sprite brick;

    //Bricks sprites array
    List<Sprite> bricksArr;
    
    //Background creation
    Background bg;
    //Speed ball/bar control variables
    byte spdX, spdY;
    byte barSpd = 7;
    //Movement control variables
    boolean mvRight;
    boolean mvLeft;
    boolean ballShoted;
    //Destroyed bricks counter
    byte counter = 0;
    //Level Counter
    byte level = 1;
    //Remain balls/lives
    byte lives = 3;

    /**
     * Call the level constructor with Width and Height parameters Initialize
     * the Bar and Bricks Sprites Verify the state of keys pressed
     *
     * @param levelWidth
     * @param levelHeight
     */
    public Arkanoid(int levelWidth, int levelHeight) {
        super(levelWidth, levelHeight);
        //instantiate a background
        bg = new Background(backgroundImg);
        //Instantiate the bar and ball in the bottom center of scene
        barSprite = new Sprite(barImg);
        barSprite.setPosition((levelWidth / 2) - (barSprite.getW() / 2), levelHeight - barSprite.getH());
        ballSprite = new Sprite(ballImg);
        centerBallOnBar();
        //Instantiate and create the array of bricks
        bricksArr = new ArrayList<>();
        drawBricks();
        
        //Control the pressed key
        keyPressedControl
                = e -> {

                    if (e.getCode().equals(KeyCode.A)) {
                        mvLeft = true;
                    }

                    if (e.getCode().equals(KeyCode.D)) {
                        mvRight = true;
                    }

                };
        //Control the released key
        keyReleasedControl
                = e -> {

                    if (e.getCode().equals(KeyCode.A)) {
                        mvLeft = false;
                    }

                    if (e.getCode().equals(KeyCode.D)) {
                        mvRight = false;
                    }
                    //Set a speed on the ball
                    if (e.getCode().equals(KeyCode.SPACE) && !ballShoted) {
                        spdX = (byte) (1 + level);
                        spdY = (byte) (-1 - level);
                        ballShoted = true;
                    }
                };

    }//

    @Override
    /**
     * Call the methods to draw a background, sprites(foreground) and HUD Also
     * returns you to the main scene when lifes reaches -1
     */
    public void preRender(GraphicsContext g) {
        drawBackground(g);
        drawForeground(g);
        drawHUD(g);
        if (lives < 0) {
            lives = 0;
            drawGameOver(g);
            gm.loadLevel(new LevelMenu(640, 480));

        }
    }

    @Override
    /**
     * Handle all the events during the game execution
     */
    public void update(float l) {
        //If ball isn't shoot, return it to the center of the bar
        if (!ballShoted) {
            this.centerBallOnBar();
        }
//if ball was shooted (pressed SPACE Key) move it
        if (ballShoted) {
            ballSprite.setSpdX(spdX);
            ballSprite.setSpdY(spdY);
            ballSprite.moveXSpd();
            ballSprite.moveYSpd();
        }
//Check the collisions betwen bricks and the ball and "destroy" the collisioned brick
        for (int i = 0; i < bricksArr.size(); i++) {
            if (CollisionUtil.rectangleColision(bricksArr.get(i), ballSprite)) {
                switch (CollisionUtil.rectangleColision(bricksArr.get(i), ballSprite, false)) {
                    case "TOP":
                        spdY *= -1;
                        counter++;
                        bricksArr.get(i).setPosition(-2000, -2000);
                        break;
                    case "BOTTOM":
                        spdY *= -1;
                        counter++;
                        bricksArr.get(i).setPosition(-2000, -2000);
                        break;
                    case "LEFT":
                        spdX *= -1;
                        counter++;
                        bricksArr.get(i).setPosition(-2000, -2000);
                        break;
                    case "RIGHT":
                        spdX *= -1;
                        counter++;
                        bricksArr.get(i).setPosition(-2000, -2000);
                        break;
                }
            }
        }

        //Next two ifs control the movement and space limits of barSprite
        if (mvRight) {
            if (barSprite.getX() <= 640 - barSprite.getW()) {
                barSprite.moveX(barSpd);
            }
        }
        if (mvLeft) {
            if (barSprite.getX() >= 0) {
                barSprite.moveX(-barSpd);
            }
        }

        //Next ifs control the direction of the ball
        if (ballSprite.getX() <= 640 - ballSprite.getW()) {
            spdX *= -1;
        }
        if (ballSprite.getX() >= 0) {
            spdX *= -1;
        }
        if (ballSprite.getY() == 0) {
            spdY *= -1;
        }
        if (CollisionUtil.rectangleColision(barSprite, ballSprite) && ballShoted) {
            spdY *= -1;
        }
        //If you lose all your lives thwen go to the main screen 
        if (ballSprite.getY() > 480 - ballSprite.getH()) {
            lives--;
            centerBallOnBar();
        }
        //If you have destroyed all the bricks then draw again the bricks and level goes up  
        if (counter==24) {
            counter=0;
            level++;
            centerBallOnBar();
            drawBricks();
        }
    }

    @Override
    public void drawBackground(GraphicsContext g) {

        bg.draw(g);
    }

    @Override
    public void drawForeground(GraphicsContext g) {
        barSprite.draw(g);
        ballSprite.draw(g);
        bricksArr.stream().forEach(spr
                -> {
            spr.draw(g);
        });
    }

    @Override
    public void drawHUD(GraphicsContext g) {
        g.setFill(Color.WHITE);
        g.fillText("Level: " + level, 500, 50);
        g.fillText("Bricks: " + counter, 500, 70);
        if (lives < 0) {
            g.fillText("Lives: " + 0, 500, 90);
        } else {
            g.fillText("Lives: " + lives, 500, 90);
        }
    }

    public void drawGameOver(GraphicsContext g) {
        //Not implemented yet
    }
/**
 * This method center de ball on the bar and stop it 
 */
    private void centerBallOnBar() {
        ballSprite.setPosition(barSprite.getX() + (barSprite.getW() / 2), barSprite.getY() - ballSprite.getH());
        spdX = 0;
        spdY = 0;
        ballShoted = false;
    }
/**
 * This methdod generate an array of bricks to print them on screen
 */
    private void drawBricks() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 8; j++) {
                brick = new Sprite(bricksImg[i]);
                brick.setPosition(j * 80, i * 29);
                bricksArr.add(brick);
            }
        }
    }

 @Override
    public void postRender(GraphicsContext g) 
    {
    }
    
}//c;ass

