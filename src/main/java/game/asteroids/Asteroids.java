package game.asteroids;

import com.bitless.graphicentity.Background;
import com.bitless.graphicentity.Sprite;
import com.bitless.level.BaseLevel;
import com.bitless.ntfc.LevelDrawable;
import com.bitless.util.CollisionUtil;
import com.bitless.util.ImageUtil;
import com.bitless.util.MathUtil;
import com.bitless.util.SpritetUtil;
import demolevel.LevelMenu;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.image.Image;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;

/**
 * Game Asteroids The main objective of this game is to destroy Asteroids with a
 * spaceship The spaceships has 3 bullets and when a big asteroid get an impact
 * of the bullet will split in 2 small asteroids The spaceship can move around
 * the screen and when a limit is reached the spaceship will appear in the
 * opposite site The player has 3 lives per level.
 *
 * @author PavulZavala
 * @author Victor Orozco
 * @author Aura Leyva
 * @author Francisco Arellano
 * @author Carlos Ramirez
 * @author Pedro Balleza
 */
public class Asteroids extends BaseLevel
        implements LevelDrawable {

    //Calling images for every sprite in the scene
    Image shipImg = ImageUtil.getImage("game/asteroids/spaceship.png");
    Image bigAsteroidImg = ImageUtil.getImage("game/asteroids/big_asteroid.png");
    Image smallAsteroidImg = ImageUtil.getImage("game/asteroids/small_asteroid.png");
    Image backgroundImg = ImageUtil.getImage("game/asteroids/background.png");
    Image bulletImg = ImageUtil.getImage("game/asteroids/bullet.png");
    //Sprite variables
    Sprite shipSprite;
    //Sprite bigAsteroidSprite;
    List<Sprite> smallAsteroidList;
    //Sprite smallAsteroidSprite2;
    List<Sprite> bulletSpriteArr;
    List<Sprite> asteroidList;
    Sprite bulletSprite;
    Sprite smallAsteroidSprite;

    Background bg;
    //Variables
    byte bulletCount = 0;
    byte spdBigAsteroid = 2;
    float spdShip = 0;
    byte levelUpCount = 0;
    //HUD variables
    byte counter = 0;
    byte level = 1;
    byte lives = 5;
    //ship rotation and movement variables, clock wise and anti clock
    boolean rotateShipClk;
    boolean rotateShipAntiClk;
    boolean mvShipForward;

    /**
     * Call the level constructor with Width and Height parameters to Initialize
     * the game
     *
     * @param levelWidth
     * @param levelHeight
     */
    public Asteroids(int levelWidth, int levelHeight) 
    {
        super(levelWidth, levelHeight);
        bg = new Background(backgroundImg);
        shipSprite = new Sprite(shipImg);
        shipSprite.setPosition(276, 350);
        //Asteroids-bullets arrays initialization
        bulletSpriteArr = new ArrayList<>();
        smallAsteroidList = new ArrayList<>();
        asteroidList = new ArrayList<>();

        //Filling the ArrayList with the asteroids
        for (int i = 0; i < 2; i++) {
            asteroidList.add(this.createAsteroid());
        }

        //Filling the ArrayList with small asteroids
        for (int i = 0; i < (asteroidList.size() * 2); i++) {
            smallAsteroidSprite = new Sprite(smallAsteroidImg);
            smallAsteroidSprite.setVisible(false);
            smallAsteroidSprite.setPosition(-200, -200);

            smallAsteroidList.add(smallAsteroidSprite);
        }

        //Filling the ArrayList with bullets
        for (int i = 0; i < 3; i++) {
            bulletSprite = new Sprite(bulletImg);
            bulletSprite.setVisible(false);
            bulletSprite.setPosition(-200, -200);
            bulletSpriteArr.add(bulletSprite);
        }

        //Controling the pressed key
        keyPressedControl
                = e -> {

                    if (e.getCode().equals(KeyCode.A)) {
                        rotateShipAntiClk = true;
                    }

                    if (e.getCode().equals(KeyCode.D)) {
                        rotateShipClk = true;
                    }

                    if (e.getCode().equals(KeyCode.W)) {
                        mvShipForward = true;
                    }

                    /**
                     * When space key is pressed and there are 3 or less bullets
                     * on the screen then the bullet get visible and came from
                     * the spaceship and it will take the same angle of the
                     * spaceship.
                     */
                    if (e.getCode().equals(KeyCode.SPACE)) {
                        if (bulletCount < 3) {
                            if (!bulletSpriteArr.get(bulletCount).isVisible()) {
                                bulletSpriteArr.get(bulletCount).setVisible(true);
                                bulletSpriteArr.get(bulletCount).setPosition(shipSprite.getX() + (shipSprite.getW() / 2) - (bulletSprite.getW() / 2),
                                        shipSprite.getY() + (shipSprite.getH() / 2) - (bulletSprite.getH() / 2));
                                SpritetUtil.moveToAngle(bulletSpriteArr.get(bulletCount), shipSprite.getAngle(), 5, false);
                                bulletCount++;
                            }
                        }
                    }
                };

        //Control the released key
        keyReleasedControl
                = e -> {

                    if (e.getCode().equals(KeyCode.A)) {
                        rotateShipAntiClk = false;
                    }

                    if (e.getCode().equals(KeyCode.D)) {
                        rotateShipClk = false;
                    }
                    if (e.getCode().equals(KeyCode.W)) {
                        mvShipForward = false;
                    }

                };
        //Instantiate the collider
//        CollisionUtil = CollisionManager.getInstance();

    }

    @Override
    /**
     * Call the methods to draw a background, sprites(foreground) and HUD Also
     * draws a GAME OVER HUD when lifes reaches -1
     */
    public void preRender(GraphicsContext g) {
        drawBackground(g);
        drawForeground(g);
        drawHUD(g);
        if (lives < 0) {
            lives = 0;
            drawGameOver(g);
            gm.loadLevel(new LevelMenu(640, 480));
        }
    }

    @Override
    /**
     * Handle all the events during the game execution
     */
    public void update(float l) {
        /**
         * In this loop we control the collisions between the bullets and the
         * asteroids. Generate small asteroids when the asteroid collide Reset
         * the position of the asteroid when it collides with a bullet and it
         * hasn't small asteroids.
         *
         */
        for (int i = 0; i < bulletSpriteArr.size(); i++) {
            for (int j = 0; j < asteroidList.size(); j++) {
                if (CollisionUtil.circleCollision(bulletSpriteArr.get(i), asteroidList.get(j), false)) {
                    asteroidList.get(j).setVisible(false);

                    smallAsteroidList.get(j * 2).setPosition(asteroidList.get(j).getX(), asteroidList.get(j).getY());
                    SpritetUtil.moveToAngle(smallAsteroidList.get(j * 2), asteroidList.get(j).getAngle() + 45, 2, true);
                    smallAsteroidList.get(j * 2).setVisible(true);

                    smallAsteroidList.get((j * 2) + 1).setPosition(asteroidList.get(j).getX(), asteroidList.get(j).getY());
                    SpritetUtil.moveToAngle(smallAsteroidList.get((j * 2) + 1), asteroidList.get(j).getAngle() - 45, 2, true);
                    smallAsteroidList.get(j * 2 + 1).setVisible(true);

                    asteroidList.get(j).setPosition(-2000, -2000);

                    if (!smallAsteroidList.get((j * 2) + 1).isVisible() && !smallAsteroidList.get(j * 2).isVisible()) {
                        repositionAsteroid(asteroidList.get(j));
                    }

                    bulletSpriteArr.get(i).setSpdX(0);
                    bulletSpriteArr.get(i).setSpdY(0);
                    bulletSpriteArr.get(i).setVisible(false);
                    bulletSpriteArr.get(i).setPosition(-200, -200);
                    bulletCount--;
                    if (bulletCount < 0) {
                        bulletCount = 0;
                    }

                    counter++;
                    levelUpCount++;

                }
            }
        }

        /**
         * In this loop we control the collisions between the bullets and the
         * small asteroids.
         */
        for (int i = 0; i < bulletSpriteArr.size(); i++) {
            for (int j = 0; j < smallAsteroidList.size(); j++) {
                if (CollisionUtil.circleCollision(bulletSpriteArr.get(i), smallAsteroidList.get(j), true)) {
                    smallAsteroidList.get(j).setVisible(false);
                    smallAsteroidList.get(j).setPosition(0, 0);
                    bulletSpriteArr.get(i).setSpdX(0);
                    bulletSpriteArr.get(i).setSpdY(0);
                    bulletSpriteArr.get(i).setVisible(false);
                    bulletSpriteArr.get(i).setPosition(-200, -200);
                    bulletCount--;
                    if (bulletCount < 0) {
                        bulletCount = 0;
                    }
                    counter++;
                    levelUpCount++;
                }
            }
        }

        //In the next if's we control the spaceship rotation
        if (rotateShipAntiClk) {
            shipSprite.setAngle(shipSprite.getAngle() - 4);

        }
        if (rotateShipClk) {
            shipSprite.setAngle(shipSprite.getAngle() + 4);
        }

        //Control the spaceship speed
        if (mvShipForward) {
            if (spdShip < 3) {
                spdShip += 0.02;
            }
            SpritetUtil.moveToAngle(shipSprite, shipSprite.getAngle(), spdShip, false);
        }
        if (!mvShipForward) {
            SpritetUtil.moveToAngle(shipSprite, shipSprite.getAngle(), spdShip, false);
            if (spdShip > 0) {
                spdShip -= 0.02;
            }
            if (spdShip < 0) {
                spdShip = 0;
            }
        }
        //Set the speed movement of the sprites
        shipSprite.moveXSpd();
        shipSprite.moveYSpd();

        bulletSpriteArr.stream().forEach(spr
                -> {
            spr.moveXSpd();
            spr.moveYSpd();
        });

        smallAsteroidList.stream().forEach(spr
                -> {
            spr.moveXSpd();
            spr.moveYSpd();
        });

        asteroidList.stream().forEach(spr -> {
            spr.moveXSpd();
            spr.moveYSpd();
        });

        //Next 4 ifs control the movement and space limits of the spaceship
        if (shipSprite.getX() > levelWidth) {
            shipSprite.setPosition(0 - shipSprite.getW(), shipSprite.getY());
        }
        if (shipSprite.getX() < 0 - shipImg.getWidth()) {
            shipSprite.setPosition(levelWidth, shipSprite.getY());
        }
        if (shipSprite.getY() > levelHeight ) {
            shipSprite.setPosition(shipSprite.getX(), 0 - shipSprite.getW());
        }
        if (shipSprite.getY() < 0 - shipImg.getWidth()) {
            shipSprite.setPosition(shipSprite.getX(), levelHeight );
        }

        /**
         * This loop handles if the bullets are inside the screen
         */
        for (int i = 0; i < 3; i++) {
            if (!CollisionUtil.isInside(
                    bulletSpriteArr.get(i).getX(), bulletSpriteArr.get(i).getY(),
                    bulletSpriteArr.get(i).getW(), bulletSpriteArr.get(i).getH(),
                    0, 0, this.levelWidth, this.levelHeight) && bulletSpriteArr.get(i).isVisible()) {
                bulletSpriteArr.get(i).setSpdX(0);
                bulletSpriteArr.get(i).setSpdY(0);
                bulletSpriteArr.get(i).setVisible(false);
                bulletSpriteArr.get(i).setPosition(-200, -200);
                bulletCount--;
                if (bulletCount < 0) {
                    bulletCount = 0;
                }
            }
        }
        
        /**
         * This loops handle if small asteroids are inside the screen
         */
        for (int i = 0; i < smallAsteroidList.size(); i++) {
            if (!CollisionUtil.isInside(
                    smallAsteroidList.get(i).getX(), smallAsteroidList.get(i).getY(),
                    smallAsteroidList.get(i).getW(), smallAsteroidList.get(i).getH(),
                    0, 0, this.levelWidth, this.levelHeight)) {
                smallAsteroidList.get(i).setPosition(-2000, -2000);
                smallAsteroidList.get(i).setVisible(false);

            }
            if (CollisionUtil.rectangleColision(smallAsteroidList.get(i), shipSprite)) {
                this.shipSprite.setPosition(276, 350);
                this.lives--;
                smallAsteroidList.get(i).setVisible(false);
                smallAsteroidList.get(i).setPosition(-2000, -2000);
            }
        }

        /**
         * If any asteroid collisions with the spaceship live will be decreased
         * and the asteroid and spaceship are going to be repositioned. If any
         * asteroid gets out of the screen it will start again
         */
        for (int i = 0; i < asteroidList.size(); i++) {
            if (!CollisionUtil.isInside(
                    asteroidList.get(i).getX(), asteroidList.get(i).getY(),
                    asteroidList.get(i).getW(), asteroidList.get(i).getH(),
                    -100, -100, this.levelWidth + 100, this.levelHeight + 100)
                    && !smallAsteroidList.get(i * 2).isVisible() && !smallAsteroidList.get((i * 2) + 1).isVisible()) {

                repositionAsteroid(asteroidList.get(i));

            }
            if (CollisionUtil.rectangleColision(asteroidList.get(i), shipSprite)) {
                this.shipSprite.setPosition(276, 350);
                this.lives--;
                repositionAsteroid(asteroidList.get(i));
            }
        }

    }

    @Override
    public void drawBackground(GraphicsContext g) {

        bg.draw(g);
    }

    @Override
    public void drawForeground(GraphicsContext g) {
        shipSprite.draw(g);
        bulletSpriteArr.stream().forEach(spr
                -> {
            spr.draw(g);
        });

        asteroidList.stream().forEach(spr
                -> {
            spr.draw(g);
        });

        smallAsteroidList.stream().forEach(spr
                -> {
            spr.draw(g);
        });

    }

    @Override
    public void drawHUD(GraphicsContext g) {
        g.setFill(Color.WHITE);
        g.fillText("Level: " + level, 500, 50);
        g.fillText("Asteroids: " + counter, 500, 70);
        if (lives < 0) {
            g.fillText("Lives: " + 0, 500, 90);
        } else {
            g.fillText("Lives: " + lives, 500, 90);
        }
    }

    public void drawGameOver(GraphicsContext g) {
        g.setFill(Color.RED);
        g.fillText("GAME OVER", levelWidth / 2.4, levelHeight / 2);
    }

    /**
     * This method repositionates randomly the asteroid around the screen
     */
    public void repositionAsteroid(Sprite s) {
        int place = (int) MathUtil.getRandomRange(0, 4);
        switch (place) {
            case 0:
                s.setPosition(levelWidth + 40, MathUtil.getRandomRange(0, levelHeight));
                break;
            case 1:
                s.setPosition(MathUtil.getRandomRange(0, levelWidth), levelHeight + 40);
                break;
            case 2:
                s.setPosition(-40, MathUtil.getRandomRange(0, levelHeight));
                break;
            case 3:
                s.setPosition(MathUtil.getRandomRange(0, levelWidth), -40);
                break;
        }
        s.setVisible(true);
        SpritetUtil.moveTo(s, 
                shipSprite.getX() + shipSprite.getW()/2,
                shipSprite.getY() + shipSprite.getH()/2, 2);
    }

    /**
     * This method returns an asteroid with a random position on the screen
     * directed to the spaceship
     */
    public Sprite createAsteroid() {
        Sprite s = new Sprite(bigAsteroidImg);
        int place = (int) MathUtil.getRandomRange(0, 4);
        switch (place) {
            case 0:
                s.setPosition(levelWidth+ 40, MathUtil.getRandomRange(0, levelHeight));
                break;
            case 1:
                s.setPosition(MathUtil.getRandomRange(0, levelWidth), levelHeight + 40);
                break;
            case 2:
                s.setPosition(-40, MathUtil.getRandomRange(0, levelHeight));
                break;
            case 3:
                s.setPosition(MathUtil.getRandomRange(0, levelHeight), -40);
        }
        SpritetUtil.moveTo(s, 
                shipSprite.getX() + shipSprite.getW()/2,
                shipSprite.getY() + shipSprite.getH()/2, 2);
        s.setVisible(true);
        return s;
    }

 @Override
    public void postRender(GraphicsContext g) 
    {
    }
    
}//c;ass

