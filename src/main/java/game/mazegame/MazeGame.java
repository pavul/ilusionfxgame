package game.mazegame;

import demolevel.LevelMenu;
import com.bitless.graphicentity.Sprite;
import com.bitless.graphicentity.Tile;
import com.bitless.level.BaseLevel;
import com.bitless.ntfc.Initiable;
import com.bitless.ntfc.LevelDrawable;
import com.bitless.util.CollisionUtil;
import com.bitless.util.ImageUtil;
import com.bitless.util.TileUtil;
import java.util.List;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;

/**
 * This level is a maze game. It has a labyrinth and the purpose of the game is
 * to lead the ball to the finish, marked with a flag. This level uses sprites,
 * movement control and collitions.
 *
 *
 * @author Pavul Zavala
 * @author Aura Leyva
 */
public class MazeGame extends BaseLevel
        implements LevelDrawable, Initiable {

    //Calling images for the sprites 
    Image playerImg = new Image("game/mazegame/ball.png");
    Image finishImg = new Image("game/mazegame/flag.png");
    Image tileImg[] = ImageUtil.getImageFrames(3, 32, 32, "game/mazegame/tiles2.png");

    //Sprite variables
    Sprite player;
    Sprite finish;

    //Movement control variables
    boolean mvRight;
    boolean mvLeft;
    boolean mvTop;
    boolean mvBottom;

    //Speed of the player sprite movement
    int spd = 2;

    //Columns and Rows for the tiles
    final byte COLS = 20;
    final byte ROWS = 15;

    //Tile sprites array
    List<Tile> tileList;
    List<Tile> colTileList;

    /**
     * Call the level constructor with Width and Height parameters
     *
     * @param levelWidth
     * @param levelHeight
     */
    public MazeGame(int levelWidth, int levelHeight) {
        super(levelWidth, levelHeight);

        /**
         * Set all image resources
         */
        initImages();

        /**
         * Set all backgrounds used in this level
         */
        initBackground();

        /**
         * Set all sprites
         */
        initSprite();

        //Control the pressed key
        keyPressedControl
                = e -> {

                    System.err.println("key pressed at: " + e.getCode());

                    if (e.getCode().equals(KeyCode.A)) {
                        mvLeft = true;
                    }

                    if (e.getCode().equals(KeyCode.D)) {
                        mvRight = true;
                    }

                    if (e.getCode().equals(KeyCode.W)) {
                        mvTop = true;
                    }

                    if (e.getCode().equals(KeyCode.S)) {
                        mvBottom = true;
                    }

                };

        //Control the released key
        keyReleasedControl
                = e -> {

                    if (e.getCode().equals(KeyCode.A)) {
                        mvLeft = false;
                    }

                    if (e.getCode().equals(KeyCode.D)) {
                        mvRight = false;
                    }

                    if (e.getCode().equals(KeyCode.S)) {
                        mvBottom = false;
                    }

                    if (e.getCode().equals(KeyCode.W)) {
                        mvTop = false;
                    }
                };

    }//

    /**
     * Handle all the events during the game execution
     */
    @Override
    public void update(float l) {

        //Next 4 ifs control the movement and space limits of the player
        if (mvRight) {
            if (player.getX() <= levelWidth - player.getW()) {
                player.moveX(spd);
            }
        }
        if (mvLeft) {
            if (player.getX() > 0) {
                player.moveX(-spd);
            }
        }

        if (mvTop) {
            if (player.getY() > 0) {
                player.moveY(-spd);
            }
        }

        if (mvBottom) {
            if (player.getY() <= levelHeight - player.getH()) {
                player.moveY(spd);
            }
        }

        if (CollisionUtil.rectangleColision(player, finish)) {
            gm.loadLevel(new LevelMenu(640, 480));
        }

        /**
         * Check if there's a collision between player and tiles 
         *
         */
        colTileList.forEach(t
                -> {
            CollisionUtil.rectangleColision(player, t, false);   
                    
        });

//        colTileList.forEach(t
//                -> {
//            if (CollisionUtil.rectangleColision(player, t, false).equals("TOP")) {
//                player.moveY(-1);
//            }
//        });
//
//        colTileList.forEach(t
//                -> {
//            if (CollisionUtil.rectangleColision(player, t, false).equals("LEFT")) {
//                player.moveX(-1);
//            }
//        });
//
//        colTileList.forEach(t
//                -> {
//            if (CollisionUtil.rectangleColision(player, t, false).equals("RIGHT")) {
//                player.moveX(1);
//            }
//        });

    }//update

    @Override
    public void preRender(GraphicsContext g) {

        drawBackground(g);

        drawForeground(g);

        drawHUD(g);

       
    }//

    /**
     * Call the methods to draw background, sprites(foreground)
     *
     */
    @Override
    public void drawBackground(GraphicsContext g) {

        g.setFill(Color.WHITE);
        g.fillRect(0, 0, levelWidth, levelHeight);

        TileUtil.drawTiles(
                g,
                tileImg,
                tileList);

    }

    @Override
    public void drawForeground(GraphicsContext g) {
        player.draw(g);
        finish.draw(g);

    }//

    @Override
    public void drawHUD(GraphicsContext g) {

    }

    @Override
    public void initData() {

    }//

    @Override
    public void initSound() {
    }//

    @Override
    public void initSprite() {
        player = new Sprite(playerImg);
        player.setPosition(0, 0);

        finish = new Sprite(finishImg);
        finish.setPosition(600, 408);

    }//

    @Override
    public void initBackground() {

        int[] bgTiles
                = {
                    0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                    1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
                    1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                    1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1,
                    1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 0, 1,
                    1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
                    1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                    1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1,
                    1, 0, 1, 1, 0, 1, 0, 1, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1, 1,
                    1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 1, 1,
                    1, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 1,
                    1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 1,
                    1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0,
                    1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};

        int[] colTiles
                = {
                    0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                    1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
                    1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                    1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1,
                    1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 0, 1,
                    1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1,
                    1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                    1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1,
                    1, 0, 1, 1, 0, 1, 0, 1, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1, 1,
                    1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 1, 1,
                    1, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 1,
                    1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 1,
                    1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0,
                    1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};

        //Tiles to draw
        tileList = TileUtil.parseTiles(bgTiles, 32, 32, ROWS, COLS);
        System.err.println("TILE SIZE: " + tileList.size());

        //Tiles to collide
        colTileList = TileUtil.parseTiles(colTiles, 32, 32, ROWS, COLS);
        System.err.println("COLTILE SIZE: " + colTileList.size());
        
        //make disposable
        bgTiles = null;
    }//

    @Override
    public void initForeground() {
    }

    @Override
    public void initHUD() {
    }

    @Override
    public void initImages() {

    }

     @Override
    public void postRender(GraphicsContext g) 
    {
    }

}//
