
package game.tetrispavul;

import com.bitless.level.BaseLevel;
import com.bitless.ntfc.LevelDrawable;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 *
 * @author PavulZavala
 */
public class TetrisPavul extends BaseLevel
            implements LevelDrawable
    {
     
      
    private final int ROW = 20;
    private final int COL = 10;
    
    private final int SQ_SIZE = 20;
      
       
        public TetrisPavul(int levelWidth, int levelHeight) 
        {
            super(levelWidth, levelHeight);
         
            
            keyReleasedControl = e->
            {  
            
            };//keyreleased
            
            
            
            
            
            
         }//
        

        @Override
        public void update( float l ) 
        {
               
        
        }//

        @Override
        public void preRender( GraphicsContext g ) 
        {
            drawBackground(g);
          
            drawForeground(g);
            
            drawHUD(g);
        }//

    @Override
    public void drawBackground(GraphicsContext g) 
    {
        
        g.setFill(Color.BLACK);
        g.fillRect( 0, 0, levelWidth, levelHeight );
        
        
    }

    @Override
    public void drawForeground(GraphicsContext g) 
    {
            
    }

    @Override
    public void drawHUD(GraphicsContext g) 
    {
        g.setFill(Color.BLACK);
        g.fillText( "TestText" , 40, 40 );
     
    }
    
     @Override
    public void postRender(GraphicsContext g) 
    {
    }
    
    }//class

   
    