package game.app;

import com.bitless.util.GameUtil;
import javafx.application.Application;
import javafx.stage.Stage;
import demolevel.LevelMenu;

/**
 * @author pavulzavala
 */
public class Game extends Application
{

    /**
     * main launching point
     * @param args 
     */
    public static void main( String... args )
    {
        launch( args );
    }

   /**
    * this method starts the game ( Stage which contains the game, which contains the canvas)
    * @param stage
    * @throws Exception 
    */
    @Override
    public void start(Stage stage) throws Exception 
    {
        
        //stage.setTitle( "GameDemo" );
        /**
         * use this to remove windows title bars, however
         * must be used before stage.show() is called
         */
        //        stage.initStyle( StageStyle.UNDECORATED );
        
        //stage.initStyle(StageStyle.valueOf( "aaa"));
        /**
         * start the game using utility function, only passing, level width / height, 
         * the scene where all will be painted and 
         * 
         * this function startGame also implement below commented code
         */
         GameUtil.startGame( stage, new LevelMenu( 640, 480 ), 640, 480 );
         
        //        //this is where game is created/initiated
        //        //640 x 480 are widht and height of the window containing the level(s)
        //        GameManager gm = new GameManager( 640,480 );
        //          
        //        /**
        //         * levelMenu is the level containing a menu of diferent levels
        //         * this level is the same size of the game window
        //         */
        //        LevelMenu levelMenu =  new LevelMenu( 640, 480 );
        //        gm.loadLevel( levelMenu );

        //************DONT REMOVE BELOW CODE****************//
        //
        //        /**
        //         * IMPORTANT!, this code is the creation of the scene where
        //         * the GameManager will be mounted in order to initiate the game
        //         */
        //        Group root = new Group();
        //                
        //        Scene scene = new Scene( root, gm.getWidth(), gm.getHeight() );
        //        
        //        root.getChildren().add( gm );
        //        
        //        stage.setScene( scene );
        //        
        //        stage.show();
        //        
        //        //make game start
        //        gm.start();
        
    }//
  
}//class  